from django2use.route.Common import *

from django2use import settings

from django2use.panel import views

webpatterns = [
    url(r'^$',           views.HOME.BrowseView.as_view(), name='landing'),

    #url(r'^$',           views.FILE.ExploreView.as_view(), name='landing'),
    #url(r'^gallery$',    views.FILE.GalleryView.as_view(), name='gallery'),
    #url(r'^uploads$',    views.FILE.UploadsView.as_view(), name='uploads'),

    url(r'^make/chart$', views.FILE.CreatorView.as_view(), name='make_chart'),
    url(r'^make/files$', views.FILE.UploadsView.as_view(), name='make_files'),
    url(r'^make/image$', views.FILE.CreatorView.as_view(), name='make_image'),
    url(r'^make/entry$', views.FILE.TabularView.as_view(), name='make_entry'),
    url(r'^make/table$', views.FILE.TabularView.as_view(), name='make_table'),

    url(r'^view/$',      views.SHOW.HomeView.as_view(),  name='view_home'),
    url(r'^view/http$',  views.SHOW.HTTP_View.as_view(), name='view_http'),
    url(r'^view/ipfs$',  views.SHOW.IPFS_View.as_view(), name='view_ipfs'),
    url(r'^view/mqtt$',  views.SHOW.MQTT_View.as_view(), name='view_mqtt'),
    url(r'^view/xmpp$',  views.SHOW.XMPP_View.as_view(), name='view_xmpp'),

    url(r'^edit/$',      views.EDIT.HomeView.as_view(),  name='edit_home'),
    url(r'^edit/ipfs$',  views.EDIT.IPFS_View.as_view(), name='edit_ipfs'),
    url(r'^edit/json$',  views.EDIT.JSON_View.as_view(), name='edit_json'),
    url(r'^edit/rdfs$',  views.EDIT.RDFS_View.as_view(), name='edit_rdfs'),
]

urlpatterns = pattern2use(webpatterns)
