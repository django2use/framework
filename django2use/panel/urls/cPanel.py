from django2use.route.Common import *

from django2use.panel import views

webpatterns = [
    url(r'^$', views.HOME.cPanelView.as_view(), name='home'),

    path('hosting/',                   views.REST.HostingListView.as_view(), name='hosting_list'),
    path('hosting/<int:pk>/',          views.REST.HostingReadView.as_view(), name='hosting_read'),
    path('hosting/add/',               views.REST.HostingMakeView.as_view(), name='hosting_make'),
    path('hosting/<int:pk>/edit',      views.REST.HostingEditView.as_view(), name='hosting_edit'),
    #path('hosting/<int:pk>/tools',     views.REST.HostingToolView.as_view(), name='hosting_tool'),
    path('hosting/<int:pk>/delete',    views.REST.HostingKillView.as_view(), name='hosting_kill'),

    path('website/',                   views.REST.WebsiteListView.as_view(), name='website_list'),
    path('website/<int:pk>/',          views.REST.WebsiteReadView.as_view(), name='website_read'),
    path('website/add/',               views.REST.WebsiteMakeView.as_view(), name='website_make'),
    path('website/<int:pk>/edit',      views.REST.WebsiteEditView.as_view(), name='website_edit'),
    #path('website/<int:pk>/tools',     views.REST.WebsiteToolView.as_view(), name='website_tool'),
    path('website/<int:pk>/delete',    views.REST.WebsiteKillView.as_view(), name='website_kill'),
]

urlpatterns = pattern2use(webpatterns)
