from rest_framework import serializers

from .models import *

######################################################################

class HostingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'target', 'pseudo', 'passwd', 'domain',
        ]
        model = Hosting

#*********************************************************************

class WebsiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias', 'frame', 'where',
            'target', 'pseudo', 'passwd', #'domain',
        ]
        model = Website
