from django.db import models

from django2use.basic.models import *
from django2use.osint.models import *

from django.utils import timezone

#*********************************************************************

WEBSITE_TYPEs = (
    ('wp', "WordPress"),

    ('mg', "Magento"),
    ('ps', "PrestaShop"),
)

######################################################################

class Hosting(models.Model):
    owner   = models.ForeignKey(Organism, related_name='hosting', on_delete=models.CASCADE)
    alias   = models.CharField(max_length=64)

    target  = models.URLField(blank=True)
    pseudo  = models.CharField(max_length=256,blank=True)
    passwd  = models.CharField(max_length=256,blank=True)
    domain  = models.ManyToManyField(Domain, related_name='hosting', blank=True)

    def __str__(self): return str(self.alias)

#*********************************************************************

class Website(models.Model):
    owner   = models.ForeignKey(Organism, related_name='websites', on_delete=models.CASCADE)
    alias   = models.CharField(max_length=64)

    frame   = models.CharField(max_length=24,choices=WEBSITE_TYPEs,blank=True)
    where   = models.ForeignKey(Hosting, related_name='websites', on_delete=models.CASCADE)

    target  = models.URLField(blank=True)
    pseudo  = models.CharField(max_length=256,blank=True)
    passwd  = models.CharField(max_length=256,blank=True)
    #domain  = models.ManyToManyField(Domain, related_name='hosting', blank=True)

    def __str__(self): return str(self.alias)
