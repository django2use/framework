from django import forms

#*********************************************************************

from .models import *

######################################################################

class HostingForm(forms.ModelForm):
    class Meta:
        model = Hosting
        exclude = ['owner']

#*********************************************************************

class WebsiteForm(forms.ModelForm):
    class Meta:
        model = Website
        exclude = ['owner']
