from django2use.helpers import *

from .models import *

from watchman.decorators import check

################################################################################

@check
def my_check():
    return {'x': 1}

################################################################################

@job('default')
def checkup_regular():
    for entry in Domain.objects.all():
        checkup_domain.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

################################################################################

@job('default', timeout=3600)
def checkup_domain(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_machine(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_hosting(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_website(uid):
    pass
