from rest_framework import viewsets
from rest_framework import permissions

from .models import *
from .serializers import *

######################################################################

class HostingViewSet(viewsets.ModelViewSet):
    queryset = Hosting.objects.all()
    serializer_class = HostingSerializer
    permission_classes = [permissions.IsAuthenticated]

#*********************************************************************

class WebsiteViewSet(viewsets.ModelViewSet):
    queryset = Website.objects.all()
    serializer_class = WebsiteSerializer
    permission_classes = [permissions.IsAuthenticated]
