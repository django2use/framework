import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

##########################################################################

class HostingType(DjangoObjectType):
    class Meta:
        model = Hosting
        filter_fields = [
            'owner', 'alias',
            'target', 'pseudo', 'passwd', 'domain',
        ]
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class WebsiteType(DjangoObjectType):
    class Meta:
        model = Website
        filter_fields = [
            'owner', 'alias', 'frame', 'where',
            'target', 'pseudo', 'passwd', #'domain',
        ]
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_hosting = graphene.relay.Node.Field(HostingType)
    all_hosting = DjangoFilterConnectionField(HostingType)

    one_website = graphene.relay.Node.Field(WebsiteType)
    all_website = DjangoFilterConnectionField(WebsiteType)

#*********************************************************************

schema = graphene.Schema(query=Query)
