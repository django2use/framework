from django.contrib import admin

from .models import *

######################################################################

class HostingAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','target','pseudo','passwd']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Hosting, HostingAdmin)

#*********************************************************************

class WebsiteAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','where','target','pseudo','passwd']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Website, WebsiteAdmin)
