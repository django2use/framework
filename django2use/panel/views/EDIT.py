from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class HomeView(TemplateView):
    template_name = 'studio/editor.html'

################################################################################

class IPFS_View(TemplateView):
    template_name = 'editor/ipfs.html'

#*******************************************************************************

class JSON_View(TemplateView):
    template_name = 'editor/json.html'

#*******************************************************************************

class RDFS_View(TemplateView):
    template_name = 'editor/rdfs.html'

#*******************************************************************************

class UploadsView(TemplateView):
    template_name = 'editor/uploads.html'
