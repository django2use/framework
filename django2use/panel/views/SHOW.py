from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class HomeView(TemplateView):
    template_name = 'studio/browse.html'

################################################################################

class HTTP_View(TemplateView):
    template_name = 'browse/http.html'

#*******************************************************************************

class IPFS_View(TemplateView):
    template_name = 'browse/ipfs.html'

#*******************************************************************************

class MQTT_View(TemplateView):
    template_name = 'browse/mqtt.html'

#*******************************************************************************

class XMPP_View(TemplateView):
    template_name = 'browse/xmpp.html'
