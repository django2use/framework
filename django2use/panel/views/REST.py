from django2use.helpers import *

from django2use.panel.models import *
from django2use.panel.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

#*******************************************************************************

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

################################################################################

class HostingListView(ListView):
    model = Hosting
    template_name = 'hosting/list.html'
    form_class = HostingForm

class HostingReadView(DetailView):
    model = Hosting
    template_name = 'hosting/item.html'
    form_class = HostingForm

class HostingMakeView(CreateView):
    model = Hosting
    template_name = 'hosting/make.html'
    fields = ['owner','alias','target','pseudo','passwd','domain']

class HostingEditView(UpdateView):
    model = Hosting
    template_name = 'hosting/edit.html'
    fields = ['alias','target','pseudo','passwd','domain']

class HostingToolView(DetailView):
    model = Hosting
    template_name = 'hosting/tool.html'
    form_class = HostingForm

class HostingKillView(DeleteView):
    model = Hosting
    success_url = reverse_lazy('hosting_list')

#*********************************************************************

class WebsiteListView(ListView):
    model = Website
    template_name = 'website/list.html'
    form_class = WebsiteForm

class WebsiteReadView(DetailView):
    model = Website
    template_name = 'website/item.html'
    form_class = WebsiteForm

class WebsiteMakeView(CreateView):
    model = Website
    template_name = 'website/make.html'
    fields = ['owner','alias','frame','where','target','pseudo','passwd']

class WebsiteEditView(UpdateView):
    model = Website
    template_name = 'website/edit.html'
    fields = ['alias','frame','where','target','pseudo','passwd']

class WebsiteToolView(DetailView):
    model = Website
    template_name = 'website/tool.html'
    form_class = WebsiteForm

class WebsiteKillView(DeleteView):
    model = Website
    success_url = reverse_lazy('website_list')
