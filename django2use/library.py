import os,sys,io,re,time,click
from tqdm import tqdm
#from urlparse import *
import types, string, validators, csv, json, yaml
from uuid import uuid4
import subprocess, requests, socket
from contextlib import closing
import logging, traceback
from bs4 import BeautifulSoup
import math,codecs,random,tldextract
from datetime import date, time, datetime, timedelta

from pathlib import Path
import dj_database_url
from decouple import config as decoupled

#*******************************************************************************

from .defines import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

rpath = lambda *x: os.path.join(BASE_DIR, *x)
bpath = lambda *x: os.path.join(BASE_DIR, 'django2use', *x)
fpath = lambda *x: os.path.join(BASE_DIR, 'thevault', *x)

rlink = lambda *x: urljoin(MAIN_LINK,'/'.join(x))

#*******************************************************************************

from django import forms
from django.db import models
from django.db.models import Q

from django.db.models.functions import TruncDay

from django.utils import timezone

#*******************************************************************************

#from django.core.exceptions import ValidationError
#from django.core.mail import send_mail
#from django.dispatch import receiver

#from django.utils import timezone, dateformat
#from django.utils.safestring import SafeString
#from django.utils.translation import gettext_lazy as _

################################################################################

from jsonfield import JSONField
from django_better_admin_arrayfield.models.fields import ArrayField

#*******************************************************************************

from django_countries.fields import CountryField

from macaddress.fields import MACAddressField

from phonenumber_field.modelfields import PhoneNumberField

################################################################################
################################################################################

def validate_domain(value):
    if not validators.domain(value):
        raise ValidationError(_('%(value)s is not a valid domain Name'
                                ), params={'value': value})


def validate_url(value):
    if not validators.url(value):
        raise ValidationError(_('%(value)s is not a valid URL Name'),
                              params={'value': value})


def validate_short_name(value):
    regex = re.compile(r'[@!#$%^&*()<>?/\|}{~:]')
    if regex.search(value):
        raise ValidationError(_('%(value)s is not a valid short name,'
                                + ' can only contain - and _'),
                              params={'value': value})

#from django_better_admin_arrayfield.models.fields import ArrayField

#from discord_webhook import DiscordWebhook

################################################################################

from rq import Queue,Retry

from rq.decorators import job
