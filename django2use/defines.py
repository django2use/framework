#!/usr/bin/python

NOTIFICATION_TYPEs = (
    ('discord',  "Discord"),
    ('slack',    "Slack"),
)

SCANNING_FLOWs = (
    ('adr', "IP address"),
    ('dns', "Domain name"),
    ('url', "Web Link"),
)

SCANNING_TYPEs = (
    ('sub', "Subdomain Tool"),
    ('dns', "Domain Analyser"),

    ('adr', "Port Scanner"),
    ('svc', "Network Scanner"),

    ('web', "Web Site Scanner"),
    ('url', "Web Link Crawler"),
)

SCANNING_MODEs = (
    ('passive', "Passive Scaning"),
    ('active',  "Active Scaning"),

    ('nessus',  "Nessus Scan"),
)

################################################################################

SCAN_STATUS = (
    (0, "Nothing Yet"),
    (1, "Prepare"),
    (2, "Running"),
    (3, "Finishs"),
)

SCAN_MAPPING = {
    0: dict(color='unset',  icone='pause',  label="Nothing Yet"),
    1: dict(color='orange', icone='flask',  label="Preparation complete"),
    2: dict(color='blue',   icone='cogs',   label="Running programs"),
    3: dict(color='green',  icone='database', label="Task finished"),
}

###############################################################################

TOOL_LOCATION="/app/tools"

###############################################################################
# TOOLS DEFINITIONS
###############################################################################

AMASS_COMMAND = 'amass enum'
###############################################################################
# YAML CONFIG DEFINITIONS
###############################################################################

SUBDOMAIN_DISCOVERY = 'subdomain'
PORT_SCAN = 'port_scan'
VISUAL_IDENTIFICATION = 'visual_id'
DIR_FILE_SEARCH = 'dirs_file'
FETCH_URL = 'fetch_url'

USES_TOOLS = 'uses_tool'
THREAD = 'thread'
AMASS_WORDLIST = 'amass_wordlist'
AMASS_CONFIG = 'amass_config'
SUBFINDER_CONFIG = 'subfinder_config'
NAABU_RATE = 'rate'
PORT = 'port'
PORTS = 'ports'
EXCLUDE_PORTS = 'exclude_ports'
EXTENSIONS = 'extensions'
RECURSIVE = 'recursive'
RECURSIVE_LEVEL = 'recursive_level'
WORDLIST = 'wordlist'
HTTP_TIMEOUT = 'http_timeout'
SCREENSHOT_TIMEOUT = 'screenshot_timeout'
SCAN_TIMEOUT = 'scan_timeout'
EXCLUDED_SUBDOMAINS = 'excluded_subdomains'

###############################################################################
# Wordlist DEFINITIONS
###############################################################################
AMASS_DEFAULT_WORDLIST_PATH = 'wordlist/default_wordlist/deepmagic.com-prefixes-top50000.txt'


###############################################################################
# Logger DEFINITIONS
###############################################################################

CONFIG_FILE_NOT_FOUND = 'Config file not found'

###############################################################################
# Preferences DEFINITIONS
###############################################################################

SMALL = '100px'
MEDIM = '200px'
LARGE = '400px'
XLARGE = '500px'
