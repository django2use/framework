from django2use.library import *

DEBUG = decoupled('DEBUG', cast=bool, default=True)

SECRET_KEY = decoupled('APP_SECRET', default='5i830%k0$u*+@0290eq&lb!c%h3cxknj01ygyck-@el-5__y4y')

APP_NAME = decoupled('APP_NAME')
APP_HELP = decoupled('APP_HELP')

APP_CODE = decoupled('APP_CODE', default="0.0.1")
APP_FQDN = decoupled('APP_FQDN', default=None)

DEFAULT_COUNTRY = 'us'
DEFAULT_LANGAGE = 'en'

PHONENUMBER_DEFAULT_REGION = decoupled('APP_PAYS', default=DEFAULT_COUNTRY).upper()

LANGUAGE_CODE = decoupled('APP_LANG', default=DEFAULT_LANGAGE+'-'+PHONENUMBER_DEFAULT_REGION)
TIME_ZONE     = decoupled('APP_TIME', default='UTC')

DFS_CURRENCY_LOCALE = '%s_%s' % (DEFAULT_LANGAGE,DEFAULT_COUNTRY)
# Specify your base template file
#DFS_BASE_TEMPLATE = 'dfs.html'

DFS_ENABLE_ADMIN = DEBUG

USE_I18N = True
USE_TZ = True

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]

SESSION_COOKIE_DOMAIN = "."+APP_FQDN

if DEBUG:
    import os  # only if you haven't already imported this
    import socket  # only if you haven't already imported this
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS = [ip[:-1] + '1' for ip in ips] + ['127.0.0.1', '10.0.2.2']

ALLOWED_HOSTS = ['*']

LOGIN_URL='/admin/login/'
LOGOUT_URL='/admin/logout/'

AUTH_USER_MODEL = 'basic.Identity'
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

MEDIA_ROOT = fpath('media')
STATIC_ROOT = fpath('serve')

STATICFILES_DIRS = (
    fpath('asset'),
)

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

INSTALLED_APPS = [
    'channels',
        'admin_tools',
        'admin_tools.theming',
        'admin_tools.menu',
        'admin_tools.dashboard',
    #'suit',
    #'suit_rq',
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.humanize',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
    'mathfilters',
        'django_better_admin_arrayfield',
    'corsheaders',
        'django_hosts',
        'social_django',
        'filer',
    'loginas',
        #'bootstrap_toolkit',
        'compressor',
        'selectable',
        # optional, for comfortable python functions editing
        'ace_overlay',
        'adminsortable2',
        'nested_inline',
        'polymorphic',
    'import_export',
        #'versatileimagefield',
        'actstream',
        'django_gravatar',
        'easy_thumbnails',
    'debug_toolbar',
        'django2use.basic',
    'oauth2_provider',
        'rest_framework',
        'rest_framework.authtoken',
        'rest_framework_httpsignature',
        'rest_social_auth',
        'rest_framework_swagger',
    'django2use.actor',

    'web3auth.apps.Web3AuthConfig',

    'django2use.queue.mains',
    'django2use.queue.defer',
    'django2use.queue.clock',
        'subscriptions',
        'phonenumber_field',
        #'extra_views',
    #'django2use.event',
        'django_filters',
        'graphene_django',
    'django2use.knows',
    'django2use.osint',
    ##'elfinder',
        'djmoney',
        'rest_hooks',
        #'insight',
        'statusboard',
        'watchman',
    'friendship',
    ##'mptt',
    'django2use.panel',

    #'business_logic',
    #'timepiece',
    #'timepiece.contracts',
    #'timepiece.crm',
    #'timepiece.entries',
    #'timepiece.reports',
]

GRAPHENE = {
    'SCHEMA': 'django2use.graph.ql.schema',
    'SCHEMA_OUTPUT': bpath('graph', 'schema.graphql')
}

ROOT_HOSTCONF = 'django2use.route.vHosts'
ASGI_APPLICATION = 'django2use.asgi.application'
WSGI_APPLICATION = 'django2use.wsgi.application'
ROOT_URLCONF = 'django2use.route.Generic'

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': rpath('local.sqlite'),
    },
    'pgsql': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django2use',
        'USER': 'django2use',
        'PASSWORD': 'django2use',
        'HOST': 'localhost',
        'PORT': 5432,
    },
}

if 'DATABASE_URL' in os.environ:
    DATABASES['cloud'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

if not os.path.exists(DATABASES['local']['NAME']):
    os.system('touch %(NAME)s' % DATABASES['local'])

DATABASES['default'] = DATABASES.get('cloud', DATABASES['pgsql'])

RQ_QUEUES = {
    'default': {
        'URL': decoupled('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 360,
    },
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            fpath('theme'),
            fpath('views'),
            #fpath('quote'),
            fpath('pages'),
        ],
        #'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'django.template.context_processors.request',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',

                'django2use.template.conf_general',
                'django2use.template.user_avatar',
                'django2use.template.suit_menus',

                'loginas.context_processors.impersonated_session_status',


            ],
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader']),
            ],
        },
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'rest_social_auth': {
            'handlers': ['console', ],
            'level': "DEBUG",
        },
        'watchman': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}

MIDDLEWARE = [
        'django_hosts.middleware.HostsRequestMiddleware',
    'django.middleware.security.SecurityMiddleware',
        #"django_permissions_policy.PermissionsPolicyMiddleware",
        #'csp.middleware.CSPMiddleware',
        'corsheaders.middleware.CorsMiddleware',
        'whitenoise.middleware.WhiteNoiseMiddleware',
        'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
        "debug_toolbar.middleware.DebugToolbarMiddleware",
    'django_hosts.middleware.HostsResponseMiddleware',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


SOCIAL_AUTH_PIPELINE = (
    #'specs.pipeline.auto_logout',  # custom action
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    #'specs.pipeline.check_for_email',  # custom action
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    #'specs.pipeline.save_avatar',  # custom action
)

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

#SOCIAL_AUTH_POSTGRES_JSONFIELD = True  # Before
#SOCIAL_AUTH_JSONFIELD_ENABLED = True  # After

AUTHENTICATION_BACKENDS = (
    'oauth2_provider.backends.OAuth2Backend',
    'social_core.backends.open_id.OpenIdAuth',
        'social_core.backends.google.GoogleOAuth2',
        #'social_core.backends.yahoo.YahooOpenId',
        'social_core.backends.facebook.FacebookOAuth2',
        'social_core.backends.twitter.TwitterOAuth',  # OAuth1.0
    'django.contrib.auth.backends.ModelBackend',
    'web3auth.backend.Web3Backend',
)

WEB3AUTH_USER_ADDRESS_FIELD = 'username'
WEB3AUTH_USER_SIGNUP_FIELDS = ['email',]

SOCIAL_AUTH_URL_NAMESPACE = 'social'

SOCIAL_AUTH_FACEBOOK_APP = decoupled('OAUTH_FACEBOOK_APP',default='')
SOCIAL_AUTH_FACEBOOK_SECRET = decoupled('OAUTH_FACEBOOK_PKI',default='')
SOCIAL_AUTH_FACEBOOK_SCOPE = decoupled('OAUTH_FACEBOOK_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': ','.join([
        # public_profile
        'id', 'cover', 'name', 'first_name', 'last_name', 'age_range', 'link',
        'gender', 'locale', 'picture', 'timezone', 'updated_time', 'verified',
        # extra fields
        'email',
    ]),
}

SOCIAL_AUTH_GOOGLE_OAUTH2_APP = decoupled('OAUTH_GOOGLE_APP',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = decoupled('OAUTH_GOOGLE_PKI',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_PROJECT = decoupled('OAUTH_GOOGLE_KEY',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = decoupled('OAUTH_GOOGLE_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')

SOCIAL_AUTH_TWITTER_APP = decoupled('OAUTH_TWITTER_APP',default='')
SOCIAL_AUTH_TWITTER_SECRET = decoupled('OAUTH_TWITTER_KEY',default='')

SUIT_CONFIG = {
    # header
    'ADMIN_NAME': APP_NAME,
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    # forms
    'SHOW_REQUIRED_ASTERISK': True,  # Default True
    'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    'SEARCH_URL': '/admin/auth/user/',
    'MENU_ICONS': {
       'sites': 'icon-leaf',
       'auth': 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': False, # Default True
    #'MENU_EXCLUDE': (
    #    'auth.group',
    #),
    'MENU': (
        {'label': 'Access', 'icon':'icon-lock', 'models': (
            'authtoken.token','speak.bottoken',
            'social_django.association','social_django.nonce','social_django.usersocialauth',
        )},
        {'label': 'Formal', 'icon':'icon-user', 'models': (
            'basic.identity','speak.botagent',
            'auth.group','basic.organism',
        )},
        {'label': 'Entity', 'icon':'icon-briefcase', 'models': (
            'speak.botlexer','speak.botvocab',
        )},
        {'label': 'Linked', 'icon':'icon-globe', 'models': (
            'daten.backend','daten.endpoint',
        )},
        {'label': 'Device', 'icon':'icon-hdd', 'models': (
            'opsys.machine','opsys.phone','opsys.router',
        )},
        {'label': 'SysAdm', 'icon':'icon-signal', 'models': (
            'basic.backend','opsys.website',
            'scheduler.repeatablejob','scheduler.scheduledjob',
        )},
        {'label': 'Runner', 'app': 'wooey', 'icon':'icon-tasks'},
        {'label': 'Queues', 'icon':'icon-cog', 'url': '/clock/'},
    ),
    #'MENU': (
    #    'sites',
    #    {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #    {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #    {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    #),
    'LIST_PER_PAGE': 50
}

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.history.HistoryPanel',
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]

VERSATILEIMAGEFIELD_SETTINGS = {
    # The amount of time, in seconds, that references to created images
    # should be stored in the cache. Defaults to `2592000` (30 days)
    'cache_length': 2592000,
    # The name of the cache you'd like `django-versatileimagefield` to use.
    # Defaults to 'versatileimagefield_cache'. If no cache exists with the name
    # provided, the 'default' cache will be used instead.
    'cache_name': 'versatileimagefield_cache',
    # The save quality of modified JPEG images. More info here:
    # https://pillow.readthedocs.io/en/latest/handbook/image-file-formats.html#jpeg
    # Defaults to 70
    'jpeg_resize_quality': 70,
    # The name of the top-level folder within storage classes to save all
    # sized images. Defaults to '__sized__'
    'sized_directory_name': '__sized__',
    # The name of the directory to save all filtered images within.
    # Defaults to '__filtered__':
    'filtered_directory_name': '__filtered__',
    # The name of the directory to save placeholder images within.
    # Defaults to '__placeholder__':
    'placeholder_directory_name': '__placeholder__',
    # Whether or not to create new images on-the-fly. Set this to `False` for
    # speedy performance but don't forget to 'pre-warm' to ensure they're
    # created and available at the appropriate URL.
    'create_images_on_demand': True,
    # A dot-notated python path string to a function that processes sized
    # image keys. Typically used to md5-ify the 'image key' portion of the
    # filename, giving each a uniform length.
    # `django-versatileimagefield` ships with two post processors:
    # 1. 'versatileimagefield.processors.md5' Returns a full length (32 char)
    #    md5 hash of `image_key`.
    # 2. 'versatileimagefield.processors.md5_16' Returns the first 16 chars
    #    of the 32 character md5 hash of `image_key`.
    # By default, image_keys are unprocessed. To write your own processor,
    # just define a function (that can be imported from your project's
    # python path) that takes a single argument, `image_key` and returns
    # a string.
    'image_key_post_processor': None,
    # Whether to create progressive JPEGs. Read more about progressive JPEGs
    # here: https://optimus.io/support/progressive-jpeg/
    'progressive_jpeg': False
}

VERSATILEIMAGEFIELD_RENDITION_KEY_SETS = {
    'image_gallery': [
        ('gallery_large', 'crop__800x450'),
        ('gallery_square_small', 'crop__50x50')
    ],
    'primary_image_detail': [
        ('hero', 'crop__600x283'),
        ('social', 'thumbnail__800x800')
    ],
    'primary_image_list': [
        ('list', 'crop__400x225'),
    ],
    'headshot': [
        ('headshot_small', 'crop__150x175'),
    ]
}

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_yaml.parsers.YAMLParser',
        'rest_framework_xml.parsers.XMLParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',

        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework_csv.renderers.CSVRenderer',
        'rest_framework_msgpack.renderers.MessagePackRenderer',
        'rest_framework_xml.renderers.XMLRenderer',
        'rest_framework_yaml.renderers.YAMLRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework_datatables.filters.DatatablesFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_datatables.pagination.DatatablesPageNumberPagination',

    'DEFAULT_AUTHENTICATION_CLASSES': [
       'django2use.nerve.Signature.MyAuthentication',
        #'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        #'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
    ],
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'PAGE_SIZE': 50,
}

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':              'rest_framework_jwt.utils.jwt_encode_handler',
    'JWT_DECODE_HANDLER':              'rest_framework_jwt.utils.jwt_decode_handler',
    'JWT_PAYLOAD_HANDLER':             'rest_framework_jwt.utils.jwt_payload_handler',
    'JWT_PAYLOAD_GET_USER_ID_HANDLER': 'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',
    'JWT_RESPONSE_PAYLOAD_HANDLER':    'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': timedelta(seconds=300),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,

}

OAUTH2_PROVIDER = {
    'OIDC_ENABLED': True,

    # this is the list of available scopes
    'SCOPES': {
        'read': 'Read scope',
        'write': 'Write scope',
        'groups': 'Access to your groups',
    },
    'DEFAULT_SCOPES': ['read', 'write'],
}

CORS_ORIGIN_ALLOW_ALL = True

# define basic SEO settings, see
DJANGO_CHECK_SEO_SETTINGS = {
    "content_words_number": [300, 600],
    "internal_links": 1,
    "external_links": 1,
    "meta_title_length": [30, 60],
    "meta_description_length": [50, 160],
    "keywords_in_first_words": 50,
    "max_link_depth": 4,
    "max_url_length": 70,
}

# define auth data (for .htaccess files)
DJANGO_CHECK_SEO_AUTH = {}

# see https://github.com/kapt-labs/django-check-seo/issues/43 for more informations
DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS = False

# update redirect with authentication strategy with value from projectname/settings.py
DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS = DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS

# define http(s) settings (default = use https)
DJANGO_CHECK_SEO_FORCE_HTTP = False

# update http(s) settings with value from projectname/settings.py
DJANGO_CHECK_SEO_FORCE_HTTP = False

# define css selector to search content into (used for retrieving main content of the page)
DJANGO_CHECK_SEO_SEARCH_IN = {
    "type": "exclude",
    "selectors": ["header", ".cover-section", "#footer"],
}

#
DJANGO_CHECK_SEO_EXCLUDE_CONTENT = ""

CORS_ALLOWED_ORIGINS = ["https://"+APP_FQDN] + [
    "https://%s.%s" % (x,APP_FQDN) for x in (
        'oauth2','manager',
        'apis',
        'cpanel',
    )
]

if False:
    CORS_ALLOWED_ORIGIN_REGEXES = [
        r"^https://([\w\d]+)\.%s$" % APP_FQDN.replace('.','\.'),
    ]

CORS_ALLOW_METHODS = [
    "DELETE",
    "GET",
    "OPTIONS",
    "PATCH",
    "POST",
    "PUT",
]

from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = list(default_headers) + [
    "my-custom-header",
]

#CSP_DEFAULT_SRC = "self"
###CSP_OBJECT_SRC = "self"
#CSP_SCRIPT_SRC = "self"
#CSP_IMG_SRC = "self"
#CSP_PREFETCH_SRC = "self"
#CSP_MEDIA_SRC = "self"
#CSP_FONT_SRC = "self"
#CSP_FRAME_SRC = "self"
#CSP_CONNECT_SRC = "self"
#CSP_STYLE_SRC = "self"
#CSP_MANIFEST_SRC = "self"
#CSP_WORKER_SRC = "self"

##CSP_BASE_URI = "#"
#CSP_CHILD_SRC = "self"
CSP_SANDBOX = []
##CSP_NAVIGATE_TO = "#"
##CSP_FORM_ACTION = "#"
##CSP_NAVIGATE_TO = "#"

PERMISSIONS_POLICY = {
    "accelerometer": [],
    "ambient-light-sensor": [],
    "autoplay": ["self", "https://archive.org"],
    "camera": [],
    "display-capture": [],
    "document-domain": [],
    "encrypted-media": [],
    "fullscreen": [],
    "geolocation": [],
    "gyroscope": [],
    "interest-cohort": [],
    "magnetometer": [],
    "microphone": [],
    "midi": [],
    "payment": [],
    "usb": [],
}

CURRENCIES = ('USD', 'EUR', 'MAD')

DYNAMICDNS_PROVIDERS = {
    'dummy': {
        'plugin': 'django2use.xtend.DNS.Dummy',
    },
    'rackspace': {
        'plugin': 'django2use.xtend.DNS.Rackspace',
        'username': 'YOUR_USERNAME',
        'api_key': 'YOUR_API_KEY',
    },
    'digitalocean': {
        'plugin': 'django2use.xtend.DNS.DigitalOcean',
        'client_id': 'YOUR_CLIENT_ID',
        'api_key': 'YOUR_API_KEY',
    },
}
DJRADICALE_CONFIG = {
    'server': {
        'base_prefix': '/pim/',
        'realm': 'Radicale - Password Required',
    },
    'encoding': {
        'request': 'utf-8',
        'stock': 'utf-8',
    },
    'auth': {
        'type': 'custom',
        'custom_handler': 'djradicale.auth.django',
    },
    'rights': {
        'type': 'custom',
        'custom_handler': 'djradicale.rights.django',
    },
    'storage': {
        'type': 'custom',
        'custom_handler': 'djradicale.storage.django',
    },
    'well-known': {
        'carddav': '/pim/%(user)s/addressbook.vcf',
        'caldav': '/pim/%(user)s/calendar.ics',
    },
}

DJRADICALE_RIGHTS = {
    'rw': {
        'user': '.+',
        'collection': '^%(login)s/[a-z0-9\.\-_]+\.(vcf|ics)$',
        'permission': 'rw',
    },
    'rw-root': {
        'user': '.+',
        'collection': '^%(login)s$',
        'permission': 'rw',
    },
}

HOOK_EVENTS = {
    ## 'any.event.name': 'App.Model.Action' (created/updated/deleted)
    #'book.added':       'bookstore.Book.created',
    #'book.changed':     'bookstore.Book.updated+',
    #'book.removed':     'bookstore.Book.deleted',
    ## and custom events, no extra meta data needed
    #'book.read':         'bookstore.Book.read',
    #'user.logged_in':    None
}

#from django.templatetags.static import static

static = lambda *x: "/".join(['/static']+[y for y in x])

STATUSBOARD = {
    "INCIDENT_DAYS_IN_INDEX": 7,
    "OPEN_INCIDENT_IN_INDEX": True,
    "AUTO_REFRESH_INDEX_SECONDS": 0,
    "FAVICON_DEFAULT": static('statusboard/images/statusboard-icon-default.png'),
    "FAVICON_INDEX_DICT": {
        0: static('statusboard/images/statusboard-icon-operational.png'),
        1: static('statusboard/images/statusboard-icon-performance.png'),
        2: static('statusboard/images/statusboard-icon-partial.png'),
        3: static('statusboard/images/statusboard-icon-major.png'),
    },
}

from watchman import constants as watchman_constants

WATCHMAN_CHECKS = watchman_constants.DEFAULT_CHECKS + tuple([
    #'module.path.to.callable',
])
#WATCHMAN_TOKEN_NAME = 'custom-token-name'
#WATCHMAN_AUTH_DECORATOR = 'django.contrib.admin.views.decorators.staff_member_required'
#WATCHMAN_ERROR_CODE = 200
EXPOSE_WATCHMAN_VERSION = True

RATELIMIT_ENABLE = True
RATELIMIT_FAIL_OPEN = True
#RATELIMIT_CACHE_PREFIX = "rl:"
#RATELIMIT_VIEW = ""
RATELIMIT_IPV4_MASK = 32
RATELIMIT_IPV6_MASK = 64

#CAN_LOGIN_AS = "utils.helpers.custom_loginas"
# This will only allow admins to log in as other users:
CAN_LOGIN_AS = lambda request, target_user: request.user.is_superuser
# This will only allow admins to log in as other users, as long as
# those users are not admins themselves:
CAN_LOGIN_AS = lambda request, target_user: request.user.is_superuser and not target_user.is_superuser

LOGINAS_LOGOUT_REDIRECT_URL = '/logout-as-redirect-url'
LOGINAS_REDIRECT_URL = '/login-as-redirect-url'

DEFAULT_HOST = 'tenant' # .%s' % APP_FQDN
