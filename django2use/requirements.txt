Django>=3, <4

-r requirements.abs

django-flexible-subscriptions

django-ace
django-countries
django-jsonfield
django-location-field
django-macaddress
django-money
django-phonenumber-field
django-versatileimagefield
#django-calendarium
django-treebeard
django-gravatar2
#django-extra-views
django-friendship
django-ratelimit

djangorestframework-gis
djangorestframework-xml

validators
djradicale
tldextract

django-activity-stream
django-loginas

django-bootstrap-toolkit
django-compressor
django-ace-overlay
django-admin-sortable2
django-nested-inline
django-selectable
django-polymorphic
django-web3-auth

django-filer
django-insight
django-money
django-permissions-policy
django-rest-hooks
django-statusboard
django-watchman

#-r requirements.dep

#django-business-logic
#django-timepiece

RDFlib
ontosPy
surf

mimeparse
SPARQLWrapper

