from django2use.helpers import *

from .models import *

################################################################################

@job('default')
def checkup_regular():
    for entry in Domain.objects.all():
        checkup_domain.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

    for entry in Machine.objects.all():
        checkup_machine.delay(entry.pk)

################################################################################

@job('default', timeout=3600)
def checkup_domain(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_machine(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_hosting(uid):
    pass

#*******************************************************************************

@job('default', timeout=3600)
def checkup_website(uid):
    pass

################################################################################

@job('default', timeout=3600)
def scan_start(scan_id, *targets, **options):
    task = ScanHistory.objects.get(pk=scan_id)

    jobs = [
        scan_begin.s(task.id),
    ] + [
        scan_entry.s(task.id,acte.method) for acte in task.activity.all()
    ] + [
        scan_after.s(task.id),
    ]

    flow = celery_chain(jobs)

    data = jobs.apply_async()

################################################################################

@job('default', timeout=3600)
def scan_begin(scan_id, *arguments):
    task = ScanHistory.objects.get(pk=scan_id)

    if task.starts is None:
        task.starts = timezone.now()
        task.save()

        task.report("Started %s Scan (#%d) on target : %s" % (
            task.method,
            task.pk,
            task.target,
        ))

@job('default', timeout=3600)
def scan_after(scan_id, *arguments, **options):
    task = ScanHistory.objects.get(pk=scan_id)

    if task.finish is None:
        task.finish = timezone.now()
        task.save()

        task.report("Finished %s Scan (#%d) on target : %s" % (
            task.method,
            task.pk,
            task.target,
        ))

################################################################################

@job('default', timeout=3600)
def scan_entry(acte_id, *arguments, **options):
    acte = ScanActivity.objects.get(pk=acte_id)

    if acte.code < 1:
        acte.invokes('begin', acte.task.runner)
        acte.code = 1
        acte.when = timezone.now()
        acte.save()

        acte.task.report("Started Activity #%d (%s) for Scan #%d : %s" % (
            acte.step,
            acte.name,
            acte.task.pk,
            acte.task.target,
        ))

    if acte.code < 2:
        for belt in Toolbelt.objects.filter(flavour=acte.name, state=True):
            if False:
                from django.template import engines

                stmt = engines['django'].from_string(
                    belt.command
                ).render(**context)
            else:
                stmt = belt.command

            acte.task.report("Executing '%s' on behalf of Activity #%d (%s) for Scan #%d : %s\n$ %s" % (
                belt.alias,
                acte.step,
                acte.name,
                acte.task.pk,
                acte.task.target,
            stmt))

            acte.task.runner(stmt)

        acte.code = 2
        acte.save()

    if acte.code < 3:
        acte.invokes('after', acte.task.runner)
        acte.code = 3
        acte.ends = timezone.now()
        acte.save()

        acte.task.report("Finished Activity #%d (%s) for Scan #%d : %s" % (
            acte.step,
            acte.name,
            acte.task.pk,
            acte.task.target,
        ))

    #acte.task.evolve = acte.step
    acte.task.save()
