import validators

from django.core.management.base import BaseCommand, CommandError

from datetime import datetime
from ipaddress import ip_address

from targets.models import *
from scanner.models import *
from results.models import *

from scanner.tasks import *

class Command(BaseCommand):
    help = 'Start a new scan from the command line.'

    def add_arguments(self, parser):
        #parser.add_argument('space', type=str, help='Indicates the number of users to be created')

        parser.add_argument('target', nargs='+', type=str)

        parser.add_argument('-s', '--space', type=str, help='Define the workspace to use')

        parser.add_argument(
            '--check',
            action='store_true',
            help='Perform a Vulnerability Scan on the Results',
        )

        parser.add_argument('-c', '--comment', type=str, help='Add comments to the scan history')

    def handle(self, target, **options):
        try:
            unit = ScanHistory.objects.get(pk=target[0])
        except Workspace.DoesNotExist:
            raise CommandError('Scan History "%s" does not exist' % target[0])

        unit.prepare()

        #print("*) Start Scanning :")
        #print("")

        scan_begin(unit.pk)

        for acte in unit.activity.order_by('step'):
            print("*) %s :" % acte.name)

            scan_entry(acte.pk)

        scan_after(unit.pk)

        #print("")
        #print("#) Scanning complete.")

        #self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % space))
