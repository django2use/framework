from django2use.route.Common import *

#from django2use.osint.views import WorkspaceScanView

from django2use.osint import views

webpatterns = [
    url(r'^$', views.HOME.DeviceView.as_view(), name='home'),

    path('domain/',                    views.REST.DomainListView.as_view(),  name='domain_list'),
    path('domain/<int:pk>/',           views.REST.DomainReadView.as_view(),  name='domain_read'),
    path('domain/add/',                views.REST.DomainMakeView.as_view(),  name='domain_make'),
    path('domain/<int:pk>/edit',       views.REST.DomainEditView.as_view(),  name='domain_edit'),
    path('domain/<int:pk>/tools',      views.REST.DomainToolView.as_view(),  name='domain_tool'),
    path('domain/<int:pk>/delete',     views.REST.DomainKillView.as_view(),  name='domain_kill'),

    path('machine/',                   views.REST.MachineListView.as_view(), name='machine_list'),
    path('machine/<int:pk>/',          views.REST.MachineReadView.as_view(), name='machine_read'),
    path('machine/add/',               views.REST.MachineMakeView.as_view(), name='machine_make'),
    path('machine/<int:pk>/edit',      views.REST.MachineEditView.as_view(), name='machine_edit'),
    path('machine/<int:pk>/tools',     views.REST.MachineToolView.as_view(), name='machine_tool'),
    path('machine/<int:pk>/delete',    views.REST.MachineKillView.as_view(), name='machine_kill'),
]

urlpatterns = pattern2use(webpatterns)
