from django2use.library import *

#*********************************************************************

from django2use.basic.models import *

#PROVIDER_CHOICES = [(name, name) for name in DYNAMICDNS_PROVIDERS.keys()]

from djmoney.models.fields import MoneyField

######################################################################


######################################################################

class Product(models.Model):
    owner = models.ForeignKey(Organism, related_name='products', on_delete=models.CASCADE)
    alias = models.CharField(max_length=64, verbose_name="Uniq Slug")

    title = models.CharField(max_length=256, blank=True, verbose_name="Title")
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD', verbose_name="Price")

    logos = models.ImageField(upload_to='product/brand', blank=True, null=True, default=None, verbose_name="Image")
    #slide = ArrayField(models.ImageField(upload_to='product/slide'), blank=True, default=list, verbose_name="Image Slide")
    draft = models.BooleanField(default=True)

    ident = models.CharField(max_length=64, blank=True, verbose_name="Product ID")
    stock = models.CharField(max_length=64, blank=True, verbose_name="S.K.U")
    brand = models.CharField(max_length=256, blank=True, verbose_name="Manufacturer")

    where = CountryField(blank=True, verbose_name="Country Of Origin")
    maker = CountryField(blank=True, verbose_name="Country Of Assembly")

    links = ArrayField(models.URLField(), blank=True, default=list, verbose_name="Web Links")
    color = ArrayField(models.CharField(max_length=32), blank=True, default=list, verbose_name="Color Palette")
    units = ArrayField(models.CharField(max_length=32), blank=True, default=list, verbose_name="Size Units")

    width  = models.FloatField(blank=True, null=True, default=None, verbose_name="Box Width")
    height = models.FloatField(blank=True, null=True, default=None, verbose_name="Box Height")
    depth  = models.FloatField(blank=True, null=True, default=None, verbose_name="Box Depth")
    weight = models.FloatField(blank=True, null=True, default=None, verbose_name="Balance Weight")

    #contexts = models.JSONField(default=default_oauth, blank=True, verbose_name="OAuth2 configuration")

    def __str__(self): return str(self.title or self.alias)

#*********************************************************************

class ProductListing(models.Model):
    model = models.ForeignKey(Product, related_name='offer', on_delete=models.CASCADE)
    draft = models.BooleanField(default=True)

    title = models.CharField(max_length=256, blank=True, verbose_name="Title")
    about = models.TextField(blank=True, verbose_name="About")

    price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD', verbose_name="Price")
    where = CountryField(blank=True, verbose_name="Country Of Origin")

    color = models.CharField(max_length=32, blank=True, verbose_name="Color Palette")
    units = models.CharField(max_length=32, blank=True, verbose_name="Size Units")

    def __str__(self): return str(self.title or self.model)

PRODUCT_RELATION = (
    ('isAccessoryOrSparePartFor',"Accessory Of"),
    ('isConsumableFor',          "COnsumable For"),
    ('isRelatedTo',              "Related To"),
    ('isSimilarTo',              "Similar To"),
    ('isVariantOf',              "Variant Of"),
)

class ProductRelation(models.Model):
    source = models.ForeignKey(Product, related_name='sources', on_delete=models.CASCADE)
    flavor = models.CharField(max_length=128, choices=PRODUCT_RELATION)
    target = models.ForeignKey(Product, related_name='targets', on_delete=models.CASCADE)
