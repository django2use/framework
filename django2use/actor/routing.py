from django2use.helpers import *

from rest_framework import status

from rest_framework.decorators import api_view as drf_view, action as drf_action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from .models import *
from .serializers import *

################################################################################

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    #permission_classes = [IsAdminUser]
    serializer_class = ProductSerializer

#*******************************************************************************
