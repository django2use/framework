from rest_framework import viewsets
from rest_framework import permissions

from .models import *
from .serializers import *

######################################################################

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]
