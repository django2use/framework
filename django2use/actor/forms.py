from django import forms

#*********************************************************************

from .models import *

from django2use.basic.models import *
from django2use.basic.forms import *

######################################################################

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['owner']
