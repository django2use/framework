from django2use.helpers import *

from django2use.actor.models import *
from django2use.actor.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

################################################################################

class AccountView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/edit.html'
    form_class = AccountForm
    success_url = '/'

#*******************************************************************************

class ProfileView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm

#*******************************************************************************

class SettingView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/conf.html'
    form_class = SettingForm

################################################################################

class ElementList(ListView):
    model = Organism
    template_name = 'organism/list.html'
    form_class = OrganismForm

#*******************************************************************************

class ElementView(DetailView):
    def get_object(self, *args, **kwargs):
        resp = None

        try:
            resp = super(IdentityView,self).get_object(*args, **kwargs)
        except ValueError:
            pass

        if (resp is None):
            if self.kwargs['pk'].lower() in ['','me','self']:
                resp = self.request.user
            else:
                resp = Identity.objects.get(username=self.kwargs['pk'])

        return resp

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm

################################################################################

class ProductListView(ListView):
    model = Product
    template_name = 'product/list.html'
    form_class = ProductForm

class ProductReadView(DetailView):
    model = Product
    template_name = 'product/item.html'
    form_class = ProductForm

class ProductMakeView(CreateView):
    template_name = 'product/make.html'
    fields = [
        'owner','alias','title',
        'price','ident','stock',
        'where','brand','maker',
        'links','color','units',
        'width','height',
        'depth','weight',
    ]
    model = Product

class ProductEditView(UpdateView):
    template_name = 'product/edit.html'
    fields = [
        'owner','alias','title',
        'price','ident','stock',
        'where','brand','maker',
        'links','color','units',
        'width','height',
        'depth','weight',
    ]
    model = Product

class ProductToolView(DetailView):
    model = Product
    template_name = 'product/tool.html'
    form_class = ProductForm

class ProductKillView(DeleteView):
    model = Product
    success_url = reverse_lazy('product_list')
