from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class LandingView(TemplateView):
    template_name = 'cpanel/landing.html'

################################################################################

class AgendaView(TemplateView):
    template_name = 'cpanel/agenda.html'

#*******************************************************************************

class BillingView(TemplateView):
    template_name = 'cpanel/billing.html'

#*******************************************************************************

class ContactView(TemplateView):
    template_name = 'cpanel/contact.html'

#*******************************************************************************

class MailboxView(TemplateView):
    template_name = 'cpanel/mailbox.html'

#*******************************************************************************

from subscriptions.models import SubscriptionPlan, PlanList

class UpgradeView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'cpanel/upgrade.html'
