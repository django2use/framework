from django2use.helpers import *

from django2use.actor.models import *
from django2use.actor.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class LandingView(TemplateView):
    template_name = 'cpanel/landing.html'

################################################################################

class SpenderList(ListView):
    model = Organism
    template_name = 'organism/list.html'
    form_class = OrganismForm

#*******************************************************************************

class SpenderView(DetailView):
    def get_object(self, *args, **kwargs):
        resp = None

        try:
            resp = super(IdentityView,self).get_object(*args, **kwargs)
        except ValueError:
            pass

        if (resp is None):
            if self.kwargs['pk'].lower() in ['','me','self']:
                resp = self.request.user
            else:
                resp = Identity.objects.get(username=self.kwargs['pk'])

        return resp

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm

################################################################################

class IncomesList(ListView):
    model = Organism
    template_name = 'organism/list.html'
    form_class = OrganismForm

#*******************************************************************************

class IncomesView(DetailView):
    def get_object(self, *args, **kwargs):
        resp = None

        try:
            resp = super(IdentityView,self).get_object(*args, **kwargs)
        except ValueError:
            pass

        if (resp is None):
            if self.kwargs['pk'].lower() in ['','me','self']:
                resp = self.request.user
            else:
                resp = Identity.objects.get(username=self.kwargs['pk'])

        return resp

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm
