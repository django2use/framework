from django2use.helpers import *

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

#*******************************************************************************

from django2use.actor.models import *
from django2use.actor.forms import *

################################################################################

class AppletView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super(TemplateView, self).get_context_data(*args, **kwargs)

        return context

    template_name = 'homepage/applet.html'

#*******************************************************************************

class WalletView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super(TemplateView, self).get_context_data(*args, **kwargs)

        context['market'] = [
            dict(code='EUR',unit=10.73,name="Euro Currency"),
            dict(code='USD',unit=10.74,name="America Dollar"),
            dict(code='GBP',unit=10.75,name="Pound Sterling"),
            dict(code='JPY',unit=10.76,name="Japanese Yen"),
            dict(code='YEN',unit=10.77,name="Chinese Yen"),

            dict(code='BTC',unit=10.7,icon='bitcoin',name="Bitcoin"),
        ]

        return context

    template_name = 'homepage/wallet.html'
