from rest_framework import serializers

from .models import *

######################################################################

class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner','alias','title',
            'price','ident','stock',
            'where','brand','maker',
            'links','color','units',
            'width','height',
            'depth','weight',
        ]
        model = Product
