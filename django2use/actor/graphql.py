import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

######################################################################

class ProductType(DjangoObjectType):
    class Meta:
        model = Product
        filter_fields = [
            'owner', 'alias',
        ]
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class Query(graphene.ObjectType):
    one_product = graphene.relay.Node.Field(ProductType)
    all_product = DjangoFilterConnectionField(ProductType)

######################################################################

schema = graphene.Schema(query=Query)
