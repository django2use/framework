from django.contrib import admin

from .models import *

######################################################################

class ListingAdmin(admin.TabularInline):
    model = ProductListing

#class RelationAdmin(admin.TabularInline):
#    model = ProductRelation

#*********************************************************************

class ProductAdmin(admin.ModelAdmin):
    inlines = (
        ListingAdmin,
        #RelationAdmin,
    )

    list_display  = [
        'owner','alias','title',
        'price','ident','stock',
        'where','brand','maker',
        'links','color','units',
        'width','height',
        'depth','weight',
    ]
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Product, ProductAdmin)
