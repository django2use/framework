from django2use.route.Common import *

from django2use.actor import views

webpatterns = [
    url(r'^$', views.Home.AppletView.as_view(), name='home'),

    url(r'^mailbox$', views.Acte.MailboxView.as_view(), name='mailbox'),
    url(r'^setting$', views.REST.SettingView.as_view(), name='setting'),
    url(r'^upgrade$', views.Acte.UpgradeView.as_view(), name='upgrade'),

    #url(r'^identity$', views.REST.SettingView.as_view(), name='identity'),
    #url(r'^geospace$', views.REST.SettingView.as_view(), name='geospace'),
    #url(r'^backends$', views.REST.SettingView.as_view(), name='backends'),
    #url(r'^datasets$', views.REST.SettingView.as_view(), name='datasets'),

    #url(r'^endpoints$', views.REST.SettingView.as_view(), name='endpoints'),
    #url(r'^pipelines$', views.REST.SettingView.as_view(), name='pipelines'),

    url(r'^bookmarks$', views.REST.ElementList.as_view(), name='bookmarks'),
    url(r'^calendar$',  views.REST.ElementList.as_view(), name='agendas'),
    url(r'^mailbox$',   views.REST.ElementList.as_view(), name='mailbox'),
    url(r'^notes$',     views.REST.ElementList.as_view(), name='notes'),
    url(r'^tasks$',     views.REST.ElementList.as_view(), name='tasks'),
]

urlpatterns = pattern2use(webpatterns)
