from django2use.route.Common import *

from django2use.actor import views

webpatterns = [
    url(r'^$', views.Home.WalletView.as_view(), name='home'),

    path(r'', include('subscriptions.urls')),

    url(r'^billing$', views.Acte.BillingView.as_view(), name='billing'),
    #url(r'^setting$', views.Acte.SettingView.as_view(), name='setting'),
    #url(r'^upgrade$', views.Acte.UpgradeView.as_view(), name='upgrade'),

    url(r'^incomes/$',          views.Bill.IncomesList.as_view(), name='incomes_list'),
    url(r'^incomes/<str:pk>/$', views.Bill.IncomesView.as_view(), name='incomes_read'),

    url(r'^spender/$',          views.Bill.SpenderList.as_view(), name='spender_list'),
    url(r'^spender/<str:pk>/$', views.Bill.SpenderView.as_view(), name='spender_read'),

    path('product/',                   views.REST.ProductListView.as_view(), name='product_list'),
    path('product/<int:pk>/',          views.REST.ProductReadView.as_view(), name='product_read'),
    path('product/add/',               views.REST.ProductMakeView.as_view(), name='product_make'),
    path('product/<int:pk>/edit',      views.REST.ProductEditView.as_view(), name='product_edit'),
    #path('product/<int:pk>/tools',     views.REST.ProductToolView.as_view(), name='product_tool'),
    path('product/<int:pk>/delete',    views.REST.ProductKillView.as_view(), name='product_kill'),
]

urlpatterns = pattern2use(webpatterns)
