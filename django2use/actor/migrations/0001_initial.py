# Generated by Django 3.2.12 on 2022-02-23 23:11

from django.db import migrations, models
import django.db.models.deletion
import django_better_admin_arrayfield.models.fields
import django_countries.fields
import djmoney.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alias', models.CharField(max_length=64, verbose_name='Uniq Slug')),
                ('title', models.CharField(blank=True, max_length=256, verbose_name='Title')),
                ('price_currency', djmoney.models.fields.CurrencyField(choices=[('EUR', 'Euro'), ('MAD', 'Moroccan Dirham'), ('USD', 'US Dollar')], default='USD', editable=False, max_length=3)),
                ('price', djmoney.models.fields.MoneyField(decimal_places=2, default_currency='USD', max_digits=14, verbose_name='Price')),
                ('logos', models.ImageField(blank=True, default=None, null=True, upload_to='product/brand', verbose_name='Image')),
                ('draft', models.BooleanField(default=True)),
                ('ident', models.CharField(blank=True, max_length=64, verbose_name='Product ID')),
                ('stock', models.CharField(blank=True, max_length=64, verbose_name='S.K.U')),
                ('brand', models.CharField(blank=True, max_length=256, verbose_name='Manufacturer')),
                ('where', django_countries.fields.CountryField(blank=True, max_length=2, verbose_name='Country Of Origin')),
                ('maker', django_countries.fields.CountryField(blank=True, max_length=2, verbose_name='Country Of Assembly')),
                ('links', django_better_admin_arrayfield.models.fields.ArrayField(base_field=models.URLField(), blank=True, default=list, size=None, verbose_name='Web Links')),
                ('color', django_better_admin_arrayfield.models.fields.ArrayField(base_field=models.CharField(max_length=32), blank=True, default=list, size=None, verbose_name='Color Palette')),
                ('units', django_better_admin_arrayfield.models.fields.ArrayField(base_field=models.CharField(max_length=32), blank=True, default=list, size=None, verbose_name='Size Units')),
                ('width', models.FloatField(blank=True, default=None, null=True, verbose_name='Box Width')),
                ('height', models.FloatField(blank=True, default=None, null=True, verbose_name='Box Height')),
                ('depth', models.FloatField(blank=True, default=None, null=True, verbose_name='Box Depth')),
                ('weight', models.FloatField(blank=True, default=None, null=True, verbose_name='Balance Weight')),
            ],
        ),
        migrations.CreateModel(
            name='ProductRelation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('flavor', models.CharField(choices=[('isAccessoryOrSparePartFor', 'Accessory Of'), ('isConsumableFor', 'COnsumable For'), ('isRelatedTo', 'Related To'), ('isSimilarTo', 'Similar To'), ('isVariantOf', 'Variant Of')], max_length=128)),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sources', to='actor.product')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='targets', to='actor.product')),
            ],
        ),
        migrations.CreateModel(
            name='ProductListing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('draft', models.BooleanField(default=True)),
                ('title', models.CharField(blank=True, max_length=256, verbose_name='Title')),
                ('about', models.TextField(blank=True, verbose_name='About')),
                ('price_currency', djmoney.models.fields.CurrencyField(choices=[('EUR', 'Euro'), ('MAD', 'Moroccan Dirham'), ('USD', 'US Dollar')], default='USD', editable=False, max_length=3)),
                ('price', djmoney.models.fields.MoneyField(decimal_places=2, default_currency='USD', max_digits=14, verbose_name='Price')),
                ('where', django_countries.fields.CountryField(blank=True, max_length=2, verbose_name='Country Of Origin')),
                ('color', models.CharField(blank=True, max_length=32, verbose_name='Color Palette')),
                ('units', models.CharField(blank=True, max_length=32, verbose_name='Size Units')),
                ('model', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offer', to='actor.product')),
            ],
        ),
    ]
