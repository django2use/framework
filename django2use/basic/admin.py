from django.contrib import admin

from .models import *

######################################################################

class LanguageAdmin(admin.ModelAdmin):
    list_display  = ['name','code','flag']
    #list_filter  = ['frame','owner__email','speak__name']
    list_editable = ['code','flag']

admin.site.register(Language, LanguageAdmin)

#*********************************************************************

class CurrencyAdmin(admin.ModelAdmin):
    list_display  = ['name','code','flag']
    #list_filter  = ['frame','owner__email','speak__name']
    list_editable = ['code','flag']

admin.site.register(Currency, CurrencyAdmin)

######################################################################

class OrganismAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','title','email','phone','birth']
    list_filter   = ['owner__username','birth']
    #list_editable = ['name']

admin.site.register(Organism, OrganismAdmin)

#*********************************************************************

#class BackendInline(admin.TabularInline):
#    model = Backend

class IdentityAdmin(admin.ModelAdmin):
    search_fields = [
        'username','email',
        'first_name','last_name',
    ]

    #inlines       = [BackendInline]

    list_display  = ['username','email','is_superuser','is_staff','first_name','last_name']
    list_filter   = ['is_superuser','is_staff']
    list_editable = ['is_superuser','is_staff','first_name','last_name']

    change_form_template = 'loginas/change_form.html'

admin.site.register(Identity, IdentityAdmin)
