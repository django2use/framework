from django.apps import AppConfig

class BasicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django2use.basic'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Identity'))
        registry.register(self.get_model('Organism'))
