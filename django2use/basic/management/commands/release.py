import validators

from django.core.management.base import BaseCommand, CommandError

from datetime import datetime
from ipaddress import ip_address

from django.contrib.sites.models import Site

from django2use.osint.models import *

from django2use import settings

class Command(BaseCommand):
    help = 'Install scanning tools defined in the application.'

    def add_arguments(self, parser):
        #parser.add_argument('space', type=str, help='Indicates the number of users to be created')

        #parser.add_argument('targets', nargs='+', type=str)

        parser.add_argument(
            '--dry',
            action='store_true',
            help="Don't actually execute anything",
        )

        #parser.add_argument('-s', '--space', type=str, help='Define the workspace to use')

    def handle(self, *args, **options):
        from django2use.route.vHosts import host_patterns

        print("*) Release Process starting :")

        for key in [x.regex.replace('.'+settings.APP_FQDN,'') for x in host_patterns]:
            dns,st = Site.objects.get_or_create(domain=key + '.' + settings.APP_FQDN)

            if st:
                print("\t-> Installing program '%(domain)s' ..." % dns.__dict__)
            else:
                print("\t=> Program '%(domain)s' installed already." % dns.__dict__)

            dns.name = key

            dns.save()

        print("#) Release Process complete.")
