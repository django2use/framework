from django2use.route.Common import *

from django.contrib import admin

from graphene_django.views import GraphQLView
from rest_framework.schemas import get_schema_view
from graph_wrap.django_rest_framework.graphql_view import graphql_view

from djradicale.views import WellKnownView

from django2use.basic import views

from django2use.osint.views import NAME as modus

apipatterns = [
    path('api/',       include('rest_framework.urls', namespace='rest_framework')),
    path('api/',       include(APIv1.urls)),

    path('nic/read/<str:domain>/', modus.dynamic_dns_read),
    path('nic/update/<str:domain>/', modus.dynamic_dns_update),
]

docpatterns = [
    path('doc/redoc/', TemplateView.as_view(
        template_name='redoc.html',
        extra_context={'schema_url':'openapi'}
    ), name='redoc'),
    path('doc/swagger/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi'}
    ), name='swagger'),
    path('doc/openapi', get_schema_view(
        title=settings.APP_NAME,
        description=settings.APP_HELP,
        version=settings.APP_CODE,
    ), name='openapi'),
]

extpatterns = [
    path('oauth2/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('action/', include('actstream.urls')),

    path('social/', include('social_django.urls', namespace='social')),
    path(r'', include('rest_social_auth.urls_session')),

    path(r'', include('subscriptions.urls')),
]

gqlpatterns = [
    path('graph/api', view=graphql_view, name='schema-graphql'),
    path('graph/gql',  csrf_exempt(GraphQLView.as_view(graphiql=False))),
    path('graph/iql', csrf_exempt(GraphQLView.as_view(graphiql=True))),
]

pimpatterns = [
    #path('ics/',       include('django2use.event.urls')),

    path(r'^' + settings.DJRADICALE_CONFIG['server']['base_prefix'].lstrip('/'), include(('djradicale.urls', 'djradicale'))),
    path(r'^\.well-known/(?P<type>(caldav|carddav))$', WellKnownView.as_view(), name='djradicale_well-known'),
]

from web3auth import urls as web3auth_urls

webpatterns = apipatterns+docpatterns+extpatterns+gqlpatterns+pimpatterns+[
    url(r'^', include(web3auth_urls)),

    #url(r'^jwt/login/', include('rest_social_auth.urls_simplejwt')),
    #url(r'^jwt/token', obtain_jwt_token),
    #url(r'^jwt/fresh', refresh_jwt_token),
    #url(r'^jwt/verif', verify_jwt_token),

    path(r'token/', include('rest_social_auth.urls_token')),

    url(r'^dashboard/session/$', views.Home.SessionView.as_view(), name='home_session'),
    url(r'^dashboard/token/$', views.Home.TokenView.as_view(), name='home_token'),
    url(r'^dashboard/jwt/$', views.Home.JWTView.as_view(), name='home_jwt'),

    url(r'^$', views.Page.LandingView.as_view(), name='home'),

    url(r'^login/?$',  views.Auth.LoginView.as_view(), name='login_session'),
    url(r'^logout/?$', views.Auth.LogoutView.as_view(), name='logout_session'),

    url(r'^user/session/', views.User.SessionDetailView.as_view(), name="current_user_session"),
    url(r'^user/token/', views.User.TokenDetailView.as_view(), name="current_user_token"),
    #url(r'^user/jwt/', views.User.JWTDetailView.as_view(), name="current_user_jwt"),

]

urlpatterns = pattern2use([
    url(r'^$',           views.Home.EngineView.as_view(), name='landing'),
]+webpatterns)
