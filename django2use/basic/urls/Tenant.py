from django2use.route.Common import *

from django2use.basic import views

webpatterns = [
    url(r'^$', views.Home.TenantView.as_view(), name='home'),

    path('organizations',    views.REST.OrganismListView.as_view(),  name='organism_list'),
    path('add',              views.REST.OrganismMakeView.as_view(),  name='organism_make'),
    path('~<int:pk>/',       views.REST.OrganismReadView.as_view(),  name='organism_read'),
    path('~<int:pk>/edit',   views.REST.OrganismEditView.as_view(),  name='organism_edit'),
    path('~<int:pk>/tools',  views.REST.OrganismToolView.as_view(),  name='organism_tool'),
    path('~<int:pk>/delete', views.REST.OrganismKillView.as_view(),  name='organism_kill'),

    path(r'contact/?',               views.REST.IdentityList.as_view(),  name='identity_list'),
    path(r'people/',                 include('friendship.urls')),

    path('profile/<str:pk>/',        views.REST.IdentityView.as_view(),  name='identity_read'),
    path('@<str:pk>',                views.REST.IdentityView.as_view(),  name='identity_view'),

    path('account',   views.REST.AccountView.as_view(),  name='account'),
    path('profile',   views.REST.ProfileView.as_view(),  name='profile'),
    path('setting',   views.REST.SettingView.as_view(),  name='setting'),
]

urlpatterns = pattern2use(webpatterns)
