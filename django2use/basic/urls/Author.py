from django2use.route.Common import *

from django2use.basic import views

webpatterns = [
    #url(r'^$', views.Home.DefaultView.as_view(), name='home'),

    url(r'^dashboard/session/$', views.Home.SessionView.as_view(), name='home_session'),
    url(r'^dashboard/token/$', views.Home.TokenView.as_view(), name='home_token'),
    url(r'^dashboard/jwt/$', views.Home.JWTView.as_view(), name='home_jwt'),

    url(r'^$', views.Page.LandingView.as_view(), name='home'),

    url(r'^logout/$', views.User.LogoutSessionView.as_view(), name='logout_session'),

    url(r'^user/session/', views.User.SessionDetailView.as_view(), name="current_user_session"),
    url(r'^user/token/', views.User.TokenDetailView.as_view(), name="current_user_token"),
    #url(r'^user/jwt/', views.User.JWTDetailView.as_view(), name="current_user_jwt"),

    url(r'^$', views.REST.AccountView.as_view(), name="home"),
]

urlpatterns = webpatterns #pattern2use(webpatterns)
