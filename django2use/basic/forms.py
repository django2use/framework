from django2use.library import *

#*********************************************************************

from .models import *

######################################################################

class AccountForm(forms.ModelForm):
    class Meta:
        fields = (
            'first_name','last_name',
            'birth',
            'logos','cover',
            'phone','sites',
        )
        model = Identity

#*********************************************************************

class ProfileForm(forms.ModelForm):
    class Meta:
        fields = (
            'currency','language','speaking',
        )
        model = Identity

#*********************************************************************

class SettingForm(forms.ModelForm):
    class Meta:
        exclude = (
            'username','email','password','groups',
            'first_name','last_name','birth',
            'logos','cover','phone','sites',
            'currency','language','speaking',
        )
        model = Identity

######################################################################

class OrganismForm(forms.ModelForm):
    class Meta:
        fields = [
            'alias','title',
            'email','phone','sites',
            'birth','logos','cover',
            #'admin','staff','crowd',
        ]
        model = Organism
