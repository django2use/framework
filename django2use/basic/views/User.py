from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

class LogoutSessionView(APIView):

    def post(self, request, *args, **kwargs):
        logout(request)
        return Response(status=status.HTTP_204_NO_CONTENT)


class BaseDetailView(generics.RetrieveAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = UserSerializer
    model = get_user_model()

    def get_object(self, queryset=None):
        return self.request.user


class SessionDetailView(BaseDetailView):
    authentication_classes = (SessionAuthentication, )


class TokenDetailView(BaseDetailView):
    authentication_classes = (TokenAuthentication, )


class JWTDetailView(SimpleJWTAuthMixin, BaseDetailView):
    pass


class KnoxDetailView(KnoxAuthMixin, BaseDetailView):
    pass
