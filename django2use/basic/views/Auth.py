from django2use.helpers import *

from django.contrib.auth import logout, get_user_model

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

class LoginView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        resp = {
            'providers': {},
        }

        maps = {
            'microsoft': 'windows',
            'coinbase':  'bitcoin',
        }

        for key in [
            'google','facebook',
            'microsoft','yahoo',
            'twitter','instagram',
            'linkedin','foursquare',
            'github','bitbucket',#'atlassian',
            'coinbase','paypal',
        ]:
            resp['providers'][maps.get(key,key)] = '%s-oauth2' % key

        for key in [
        ]:
            resp['providers'][key] = key

        return resp

    #def post(self, request, *args, **kwargs):
    #    return Response(status=status.HTTP_204_NO_CONTENT)

    template_name = 'login.html'

#*******************************************************************************

class LogoutView(APIView):
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logout(request)

        return Response(status=status.HTTP_204_NO_CONTENT)

################################################################################
