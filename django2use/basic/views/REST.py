from django2use.helpers import *

from django2use.basic.models import *
from django2use.basic.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

################################################################################

class IdentityList(ListView):
    model = Identity
    template_name = 'identity/list.html'
    form_class = ProfileForm

class IdentityView(DetailView):
    def get_object(self, *args, **kwargs):
        resp = None

        try:
            resp = super(IdentityView,self).get_object(*args, **kwargs)
        except ValueError:
            pass

        if (resp is None):
            if self.kwargs['pk'].lower() in ['','me','self']:
                resp = self.request.user
            else:
                resp = Identity.objects.get(username=self.kwargs['pk'])

        return resp

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm

#*******************************************************************************

class AccountView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/edit.html'
    form_class = AccountForm
    success_url = '/'

class ProfileView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/item.html'
    form_class = ProfileForm

class SettingView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'identity/conf.html'
    form_class = SettingForm

################################################################################

class OrganismListView(ListView):
    model = Organism
    template_name = 'organism/list.html'
    form_class = OrganismForm

class OrganismReadView(DetailView):
    model = Organism
    template_name = 'organism/item.html'
    form_class = OrganismForm

class OrganismMakeView(CreateView):
    model = Organism
    template_name = 'organism/make.html'
    fields = [
        'alias','title',
        'email','phone','sites',
        'birth','logos','cover',
        #'admin','staff','crowd',
    ]

class OrganismEditView(UpdateView):
    model = Organism
    fields = [
        'alias','title',
        'email','phone','sites',
        'birth','logos','cover',
        #'admin','staff','crowd',
    ]
    template_name = 'organism/edit.html'

class OrganismToolView(DetailView):
    model = Organism
    template_name = 'organism/tool.html'
    form_class = OrganismForm

class OrganismKillView(DeleteView):
    model = Organism
    success_url = reverse_lazy('organism_list')
