from rest_framework import serializers

from .models import *

class LanguageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Language
        fields = ['name', 'code', 'flag']

class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Language
        fields = ['name', 'code', 'flag']

class OrganismSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Organism
        fields = [
            'alias','title',
            #'email','phone','sites',
            'birth','logos','cover',
            'admin','crowd',
        ]

class IdentitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Identity
        fields = [
            'url','pk','username',#'email',
            'first_name','last_name','birth',
            'logos','cover',#'phone','sites',
            'currency','language','speaking',
        ]

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
