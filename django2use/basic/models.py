from django2use.library import *

#*********************************************************************

from django.contrib.auth.models import AbstractUser, Group

######################################################################

class Language(models.Model):
    code      = models.CharField(max_length=32, verbose_name="Language Slug")
    name      = models.CharField(max_length=128, verbose_name="Langauge Name")
    flag      = models.CharField(max_length=64, blank=True, verbose_name="Country Code")

    def __str__(self): return str(self.name)

#*********************************************************************

class Currency(models.Model):
    code      = models.CharField(max_length=32, verbose_name="Currency Slug")
    name      = models.CharField(max_length=128, blank=True, verbose_name="Currency Name")
    flag      = models.CharField(max_length=64, blank=True, verbose_name="Country Code")

    class Meta:
        verbose_name_plural = 'Currencies'

    def __str__(self): return str(self.name)

######################################################################

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)

FULL_NAME_TYPEs = [
    (x,x) for x in (
        "%(last_name)s %(first_name)s",
        "%(first_name)s, %(last_name)s",
    )
]

class Identity(AbstractUser):
    title = models.CharField(max_length=128, choices=FULL_NAME_TYPEs, blank=True, default=FULL_NAME_TYPEs[0][0])
    about = models.TextField(blank=True)
    birth = models.DateField(blank=True, verbose_name="Date of Birth", null=True, default=None)
    where = CountryField()
    roles = ArrayField(models.CharField(max_length=64), blank=True, default=list, verbose_name="Job Roles")

    @property
    def fullname(self):
        return self.title % self.__dict__

    @property
    def mugshot(self):
        if self.logos and os.path.exists(self.logos.path):
            return self.logos.url
        elif has_gravatar(self.email):
            return self.get_gravatar_url(self.email)
        else:
            return "/static/images/picture.jpg"

    logos = models.ImageField(upload_to='identity/brand', blank=True, verbose_name="Image Brand", null=True, default=None)
    cover = models.ImageField(upload_to='identity/cover', blank=True, verbose_name="Image Cover", null=True, default=None)

    phone = ArrayField(PhoneNumberField(), blank=True, default=list, verbose_name="Phone Numbers")
    sites = ArrayField(models.URLField(), blank=True, default=list, verbose_name="Websites")

    social_thumb = models.URLField(null=True, blank=True)

    #contexts = models.JSONField(default=default_oauth, blank=True, verbose_name="OAuth2 configuration")

    #balanced = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')
    currency = models.ForeignKey(Currency, related_name='traders', on_delete=models.CASCADE, blank=True, null=True)

    language = models.ForeignKey(Language, related_name='audience', on_delete=models.CASCADE, blank=True, null=True)
    speaking = models.ManyToManyField(Language, related_name='speakers', blank=True)

    class Meta:
        verbose_name = 'Identity'
        verbose_name_plural = 'Identities'

    def __str__(self): return str(self.username)

#*********************************************************************

class Organism(models.Model):
    owner = models.ForeignKey(Identity, related_name='teams', on_delete=models.CASCADE)

    alias = models.CharField(max_length=64)
    title = models.CharField(max_length=256, blank=True)

    email = ArrayField(models.EmailField(), blank=True, default=list, verbose_name="E-mails")
    phone = ArrayField(PhoneNumberField(), blank=True, default=list, verbose_name="Phone Numbers")
    sites = ArrayField(models.URLField(), blank=True, default=list, verbose_name="Websites")

    birth = models.DateField(blank=True, verbose_name="Date of Birth", null=True, default=None)
    logos = models.ImageField(upload_to='organism/brand', blank=True, verbose_name="Image Brand", null=True, default=None)
    cover = models.ImageField(upload_to='organism/cover', blank=True, verbose_name="Image Cover", null=True, default=None)

    admin = models.ManyToManyField(Identity, related_name='admin', blank=True)
    staff = models.ManyToManyField(Identity, related_name='staff', blank=True)
    crowd = models.ManyToManyField(Identity, related_name='crowd', blank=True)

    class Meta:
        verbose_name = 'Organization'
        verbose_name_plural = 'Organizations'

    def __str__(self): return self.title or self.alias
