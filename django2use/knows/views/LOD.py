from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

class ContextView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from subscriptions.models import SubscriptionPlan, PlanList

        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'linked/context.html'

#*******************************************************************************

class OntologyView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from subscriptions.models import SubscriptionPlan, PlanList

        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'linked/ontology.html'

#*******************************************************************************

class InferenceView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from subscriptions.models import SubscriptionPlan, PlanList

        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'linked/inference.html'

#*******************************************************************************

class DatasetView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from subscriptions.models import SubscriptionPlan, PlanList

        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'linked/dataset.html'
