from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

#*******************************************************************************

from owlready2 import *

import rdflib, surf

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class LandingView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from subscriptions.models import SubscriptionPlan, PlanList

        resp = {}

        resp['listing'] = [
            (entry, entry.plan_list_details.order_by('order'))
            for entry in PlanList.objects.filter(active=True)
        ]

        return resp

    template_name = 'linked/resource.html'

################################################################################

class View1(TemplateView):
    def get_context_data(self, *args, **kwargs):
        store = surf.Store(  reader='rdflib',
                    writer='rdflib',
                    rdflib_store = 'IOMemory')

        session = surf.Session(store)

        print 'Load RDF data'
        store.load_triples(source='http://www.w3.org/People/Berners-Lee/card.rdf')

        Person = session.get_class(ns.FOAF['Person'])

        all_persons = Person.all()

        print 'Found %d persons that Tim Berners-Lee knows'%(len(all_persons))
        for person in all_persons:
            print person.foaf_name.first

        #create a person object
        somebody = Person()
        somebody_else = Person()

        somebody.foaf_knows = somebody_else

    template_name = 'linked/resource.html'

#*******************************************************************************

class View2(TemplateView):
    def get_context_data(self, *args, **kwargs):
        store = surf.Store(reader = 'sparql_protocol',
                           endpoint = 'http://dbpedia.org/sparql',
                           default_graph = 'http://dbpedia.org')

        print 'Create the session'
        session = surf.Session(store, {})
        session.enable_logging = False

        PhilCollinsAlbums = session.get_class(surf.ns.YAGO['PhilCollinsAlbums'])

        all_albums = PhilCollinsAlbums.all()

        print 'Phil Collins has %d albums on dbpedia' % len(all_albums)

        first_album = all_albums.first()
        first_album.load()

        print 'All covers'
        for a in all_albums:
            if a.dbpedia_name:
                cvr = a.dbpedia_cover
                print '\tCover %s for "%s"' % (str(a.dbpedia_cover), str(a.dbpedia_name))

    template_name = 'linked/resource.html'

#*******************************************************************************

class View3(TemplateView):
    def get_rdflib_store():
        store = rdflib.plugin.get('MySQL', rdflib.store.Store)('rdfstore')

        # rdflib can create necessary structures if the store is empty
        rt = store.open(DB_CONN, create=False)
        if rt == rdflib.store.VALID_STORE:
            pass
        elif rt == rdflib.store.NO_STORE:
            store.open(DB_CONN, create=True)
        elif rt == rdflib.store.CORRUPTED_STORE:
            store.destroy(DB_CONN)
            store.open(DB_CONN, create=True)

            return store

    # mysql connection string that will be passed to rdflib's mysql plugin
    DB_CONN = 'host=localhost,user=surf,password=password,db=rdfstore'

    def get_context_data(self, *args, **kwargs):
        store = surf.Store(reader='rdflib',
                           writer='rdflib',
                           rdflib_store = get_rdflib_store())
        session = surf.Session(store)

    template_name = 'linked/resource.html'

################################################################################

class MyPerson(object):
    """ Some custom logic for foaf:Person resources. """

    def get_friends_count(self):
            return len(self.foaf_knows)

#*******************************************************************************

class View4(TemplateView):
    def get_context_data(self, *args, **kwargs):
        session.mapping[surf.ns.FOAF.Person] = MyPerson

        # Now let's test the mapping
        john = session.get_resource("http://example/john", surf.ns.FOAF.Person)

        # Is `john` an instance of surf.Resource?
        print isinstance(john, surf.Resource)
        # outputs: True

        # Is `john` an instance of MyPerson?
        print isinstance(john, MyPerson)
        # outputs: True

        # Try the custom `get_friends_count` method:
        print john.get_friends_count()
        # outputs: 0

    template_name = 'linked/resource.html'

################################################################################

class View5(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from surf.rdf import URIRef

        sess = surf.Session(surf.Store(reader="rdflib", writer="rdflib"))
        sess.default_store.add_triple(URIRef("http://s"), URIRef("http://p"), "value!")
        # <rdflib.sparql.QueryResult.SPARQLQueryResult object at ...>
        sess.default_store.execute_sparql("SELECT ?s ?p ?o WHERE { ?s ?p ?o }")
        # [(rdflib.URIRef('http://s'), rdflib.URIRef('http://p'), 'value!')]
        list(sess.default_store.execute_sparql("SELECT ?s ?p ?o WHERE { ?s ?p ?o }"))

    template_name = 'linked/resource.html'

#*******************************************************************************

class View6(TemplateView):
    def get_context_data(self, *args, **kwargs):
        from surf.query import a, select
        query = select("?s", "?src")
        query.named_group("?src", ("?s", a, surf.ns.FOAF['Person']))
        # SELECT  ?s ?src  WHERE {  GRAPH ?src {  ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person>  }  }
        print unicode(query)

    template_name = 'linked/resource.html'

#*******************************************************************************

class View7(TemplateView):
    def get_context_data(self, *args, **kwargs):
        onto_path.append("/path/to/your/local/ontology/repository")
        onto = get_ontology("http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl")
        onto.load()

        with onto:
            class NonVegetarianPizza(onto.Pizza):
              equivalent_to = [
                onto.Pizza
              & ( onto.has_topping.some(onto.MeatTopping)
                | onto.has_topping.some(onto.FishTopping)
                ) ]
              def eat(self): print("Beurk! I'm vegetarian!")

    template_name = 'linked/resource.html'
