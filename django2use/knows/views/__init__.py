from django2use.helpers import *

#*******************************************************************************

from django2use.knows.models import *

################################################################################

class LandingView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super(TemplateView, self).get_context_data(*args, **kwargs)

        return context

    template_name = 'homepage/linked.html'

################################################################################

from . import NS
from . import EP

from . import OWL
from . import RDF
from . import LOD

from . import HDT
from . import SDP
from . import TPF

#*******************************************************************************

#from . import DAG
#from . import PSY
