from django2use.route.Common import *

from django2use.knows import views

webpatterns = [
    path(r'ep/', views.EP.LandingView, name='sparql_endpoint'),
    path(r'ns/', views.NS.LandingView, name='ns_vocabulary'),

    path(r'owl/', views.OWL.ontoDocsMain, name='ontodocs_main'),
    path(r'rdf/', views.RDF.LandingView, name='rdf_dj_pubby'),

    path(r'hdt/', views.HDT.LandingView, name='hdt_archives'),
    path(r'sdp/', views.SDP.LandingView, name='solid_data_pod'),
    path(r'tpf/', views.TPF.LandingView, name='triple_fragment'),

    path('context\.jsonld', views.LOD.ContextView.as_view(),  name='org_context'),
    path('ontology\.owl', views.LOD.OntologyView.as_view(),  name='org_ontology'),
    path('inference\.n3', views.LOD.InferenceView.as_view(),  name='org_inference'),
    ## trig xml rdf ttl
    path('dataset\.<str:format>', views.LOD.DatasetView.as_view(),  name='org_dataset'),
]

urlpatterns = pattern2use([
    url(r'^$',           views.LandingView.as_view(), name='landing'),
]+webpatterns)
