# Generated by Django 3.2.12 on 2022-02-16 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HistoryEntry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uri', models.CharField(max_length=350)),
                ('description', models.TextField(blank=True, verbose_name='description')),
                ('pubdate', models.DateTimeField(blank=True, null=True, verbose_name='date added')),
                ('score', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'HistoryEntry',
                'verbose_name_plural': 'HistoryEntry',
                'ordering': ['id'],
            },
        ),
    ]
