from django2use.route.Common import *

from django.contrib import admin

from graphene_django.views import GraphQLView
from rest_framework.schemas import get_schema_view
from graph_wrap.django_rest_framework.graphql_view import graphql_view

import subscriptions

webpatterns = [
    path('admin/',    admin.site.urls),
    path('queue/',    include('django2use.queue.mains.urls')),
    path('clock/',    include('django2use.queue.defer.urls')),

    path('oauth/',    include('oauth2_provider.urls', namespace='oauth2_provider')),
    #path('oauth/',    include('django2use.basic.urls.Core')),

    #path('apis/',     include('django2use.route.old.APIs')),
    path('',          include('subscriptions.urls')),

    path('graph/ql',  csrf_exempt(GraphQLView.as_view(graphiql=False))),
    path('graph/iql', csrf_exempt(GraphQLView.as_view(graphiql=True))),

    #path('',          include('django2use.basic.urls.Page')),
]

urlpatterns = pattern2use(webpatterns)
