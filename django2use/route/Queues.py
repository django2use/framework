from django2use.route.Common import *

from django.conf import settings
from django.contrib import admin

webpatterns = [
    path('clock/',    include('django2use.queue.defer.urls')),
    path('',    include('django2use.queue.mains.urls')),
]

urlpatterns = pattern2use(webpatterns)
