from django_hosts import patterns, host

from django2use.settings import APP_FQDN

host_patterns = patterns('',
    host(r'manage.%s' % APP_FQDN, 'django2use.route.Manage', name='manage'),
    host(r'queues.%s' % APP_FQDN, 'django2use.route.Queues', name='queues'),

    host(r'status.%s' % APP_FQDN, 'django2use.route.Status', name='status'),

    host(r'engine.%s' % APP_FQDN, 'django2use.basic.urls.Engine', name='engine'),
    host(r'tenant.%s' % APP_FQDN, 'django2use.basic.urls.Tenant', name='tenant'),

    host(r'cpanel.%s' % APP_FQDN, 'django2use.panel.urls.cPanel', name='cpanel'),
    host(r'browse.%s' % APP_FQDN, 'django2use.panel.urls.Browse', name='browse'),

    host(r'linked.%s' % APP_FQDN, 'django2use.knows.urls.Linked', name='linked'),
    #host(r'graphs.%s' % APP_FQDN, 'django2use.basic.urls.Graphs', name='graphs'),

    host(r'device.%s' % APP_FQDN, 'django2use.osint.urls.Device', name='device'),
    host(r'metric.%s' % APP_FQDN, 'django2use.osint.urls.Metric', name='metric'),

    host(r'applet.%s' % APP_FQDN, 'django2use.actor.urls.Applet', name='applet'),
    host(r'wallet.%s' % APP_FQDN, 'django2use.actor.urls.Wallet', name='wallet'),
)
