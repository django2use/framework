from django2use.route.Common import *

from django.conf import settings
from django.contrib import admin

admin.site.site_header = "Digital-Flood Manager"

import subscriptions

webpatterns = [
    #path('logic/',    include('business_logic.urls')),
    path('nested/',   include('nested_admin.urls')),
    path('',          include('loginas.urls')),
    path('',          admin.site.urls),
    path('',          include('admin_tools.urls')),
]

urlpatterns = pattern2use(webpatterns)
