from django2use.helpers import *

from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from django2use import settings

from django.contrib.admin.views.decorators import staff_member_required

def pattern2use(urlpatterns):
    from django2use.basic.views.Page import gentella_html

    urlpatterns += [
        url(r'^.*\.html', gentella_html, name='gentella'),
        #url(r'^$', views.Home.DefaultView.as_view(), name='home'),
    ]

    if settings.DEBUG:
        import debug_toolbar

        urlpatterns += [
            path('__debug__/', include(debug_toolbar.urls)),
        ]

    urlpatterns += static(settings.MEDIA_URL,  document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    return urlpatterns
