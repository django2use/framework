from django2use.route.Common import *

#from django2use.osint import views

webpatterns = [
    #url(r'^$', views.HOME.MetricView.as_view(), name='home'),

    path(r'',         include('statusboard.urls')),
    #path(r'insight/', include('insight.urls')),
    path(r'watcher/', include('watchman.urls')),

    #path(r'board', include('statusboard.urls')),
    #path(r'hooks/', include('rest_hooks.urls')),
    #path(r'sight/', include('insight.urls')),
    #path(r'watch/', include('watchman.urls')),
]

urlpatterns = pattern2use([
    #url(r'^$',           views.Home.EngineView.as_view(), name='landing'),
]+webpatterns)
