import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import *

######################################################################

class DomainType(DjangoObjectType):
    class Meta:
        model = Domain
        filter_fields = [
            'owner', 'alias',
            'start', 'finis',
            'price',
        ]
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class MachineType(DjangoObjectType):
    class Meta:
        model = Machine
        filter_fields = [
            'owner', 'alias',
            'hw_addr', 'ipv4addr', 'ipv6addr',
            'username', 'password',
        ]
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_domain = graphene.relay.Node.Field(DomainType)
    all_domain = DjangoFilterConnectionField(DomainType)

    one_machine = graphene.relay.Node.Field(MachineType)
    all_machine = DjangoFilterConnectionField(MachineType)

#*********************************************************************

schema = graphene.Schema(query=Query)
