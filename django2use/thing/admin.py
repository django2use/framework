from django2use.helpers import *

from .models import *

######################################################################

class DomainAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','start','finis','price']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Domain, DomainAdmin)

#*********************************************************************

class MachineAdmin(admin.ModelAdmin):
    list_display = ('owner', 'alias', 'ip4_addr', 'ip6_addr', 'provider', 'hostname', 'last_time',)
    ordering = ('owner__alias', 'last_time', 'provider')
    #list_filter = ('provider', 'last_time')

admin.site.register(Machine, MachineAdmin)

################################################################################

class ScanningAdmin(admin.ModelAdmin):
    list_display  = ['alias','label','helps','state','flavour']
    list_filter   = ['state','flavour']

admin.site.register(Toolbelt, ScanningAdmin)

#*******************************************************************************

class ConfigAdmin(admin.ModelAdmin):
    list_display = ['name','tool','flag','path']
    list_filter  = ['tool__alias']

admin.site.register(Configuration, ConfigAdmin)
