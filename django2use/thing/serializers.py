from django2use.helpers import *

from rest_framework import status

from rest_framework.decorators import api_view as drf_view, action as drf_action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from .models import *

######################################################################

class DomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'start', 'finis',
            'price',
        ]
        model = Domain

#*********************************************************************

class MachineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'hw_addr', 'ipv4addr', 'ipv6addr',
            'username', 'password',
        ]
        model = Machine

################################################################################

class ToolbeltSerializer(serializers.ModelSerializer):
    class Meta:
        model = Toolbelt
        exclude = ['flavour','command','install']

#*******************************************************************************
