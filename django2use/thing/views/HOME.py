from django2use.helpers import *

from django2use.osint.models import *
from django2use.osint.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

#*******************************************************************************

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

################################################################################

class MetricView(TemplateView):
    template_name = 'homepage/metric.html'

#*******************************************************************************

class DeviceView(TemplateView):
    template_name = 'homepage/device.html'
