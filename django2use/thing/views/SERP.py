from django2use.helpers import *

from django2use.osint.models import *
from django2use.osint.forms import *

from django2use.panel.models import *
from django2use.panel.forms import *

#*******************************************************************************

from django2use.osint.checks import site
from django2use.osint.checks_list import launch_checks

################################################################################

class GenericView(DetailView):
    model = Domain
    template_name = 'tools/seo.html'
    form_class = DomainForm

    def get_object(self, *args, **kwargs):
        resp = {
            'fqdn': self.kwargs['pk'].lower(),

            'pseudo': None,
            'passwd': None,
        }

        resp['link'] = "https://%(fqdn)s/" % resp

        return resp

    def get_context_data(self, *args, **kwargs):
        context = super(GenericView, self).get_context_data(*args, **kwargs)

        # use credentials if provided (pass through .htaccess auth)
        if (
            settings.DJANGO_CHECK_SEO_AUTH
            and context['object']["pseudo"] is not None
            and context['object']["passwd"] is not None
        ):
            r = requests.get(
                context['object']['link'],
                auth=(
                    context['object']["pseudo"],
                    context['object']["passwd"],
                ),
                headers={"Cache-Control": "no-store"},
                allow_redirects=False,
            )
            if (
                300 < r.status_code < 400
                and settings.DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS
            ):
                r = requests.get(
                    r.headers["Location"],
                    auth=(
                        context['object']["pseudo"],
                        context['object']["passwd"],
                    ),
                )
        else:
            r = requests.get(context['object']['link'], headers={"Cache-Control": "no-store"})

        soup = BeautifulSoup(r.text, features="lxml")

        # populate class with data
        page_stats = site.Site(soup, context['object']['link'])

        # magic happens here!
        launch_checks.launch_checks(page_stats)

        # end of magic, get collected problems/warnings/success and put them inside the context
        (context["problems"], context["warnings"], context["success"]) = (
            page_stats.problems,
            page_stats.warnings,
            page_stats.success,
        )

        context["settings"] = json.dumps(settings.DJANGO_CHECK_SEO_SETTINGS, indent=4)
        context["html"] = page_stats.content
        context["text"] = page_stats.content_text
        context["keywords"] = page_stats.keywords

        nb_problems = len(context["problems"])
        nb_warnings = len(context["warnings"])

        # define some fancy-looking text here because it is wayyy to dirty with all the {% trans %} in template
        if nb_problems == 0 and nb_warnings == 0:
            context["nb_problems_warnings"] = _(
                '<strong class="green-bg">No problem</strong> was found on the page!'
            )
        else:
            if nb_problems == 0:
                context["nb_problems_warnings"] = _(
                    '<strong class="green-bg">No problem</strong> found, and '
                )
            else:
                context["nb_problems_warnings"] = '<strong class="red-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_problems} problem</strong> found, and ",
                    "{nb_problems} problems</strong> found, and ",
                    nb_problems,
                ).format(nb_problems=nb_problems)

            if nb_warnings == 0:
                context["nb_problems_warnings"] += _(
                    '<strong class="green-bg">no warning</strong> raised'
                )
            else:
                context["nb_problems_warnings"] += '<strong class="yellow-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_warnings} warning</strong> raised",
                    "{nb_warnings} warnings</strong> raised",
                    nb_warnings,
                ).format(nb_warnings=nb_warnings)

        return context

################################################################################

class DomainView(DetailView):
    model = Domain
    template_name = 'tools/seo.html'
    form_class = DomainForm

    def get_context_data(self, *args, **kwargs):
        context = super(DetailView, self).get_context_data(*args, **kwargs)

        # use credentials if provided (pass through .htaccess auth)
        if (
            settings.DJANGO_CHECK_SEO_AUTH
            and settings.DJANGO_CHECK_SEO_AUTH["user"] is not None
            and settings.DJANGO_CHECK_SEO_AUTH["pass"] is not None
        ):
            r = requests.get(
                context['object'].link,
                auth=(
                    settings.DJANGO_CHECK_SEO_AUTH["user"],
                    settings.DJANGO_CHECK_SEO_AUTH["pass"],
                ),
                headers={"Cache-Control": "no-store"},
                allow_redirects=False,
            )
            if (
                300 < r.status_code < 400
                and settings.DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS
            ):
                r = requests.get(
                    r.headers["Location"],
                    auth=(
                        settings.DJANGO_CHECK_SEO_AUTH["user"],
                        settings.DJANGO_CHECK_SEO_AUTH["pass"],
                    ),
                )
        else:
            r = requests.get(context['object'].link, headers={"Cache-Control": "no-store"})

        soup = BeautifulSoup(r.text, features="lxml")

        # populate class with data
        page_stats = site.Site(soup, context['object'].link)

        # magic happens here!
        launch_checks.launch_checks(page_stats)

        # end of magic, get collected problems/warnings/success and put them inside the context
        (context["problems"], context["warnings"], context["success"]) = (
            page_stats.problems,
            page_stats.warnings,
            page_stats.success,
        )

        context["settings"] = json.dumps(settings.DJANGO_CHECK_SEO_SETTINGS, indent=4)
        context["html"] = page_stats.content
        context["text"] = page_stats.content_text
        context["keywords"] = page_stats.keywords

        nb_problems = len(context["problems"])
        nb_warnings = len(context["warnings"])

        # define some fancy-looking text here because it is wayyy to dirty with all the {% trans %} in template
        if nb_problems == 0 and nb_warnings == 0:
            context["nb_problems_warnings"] = _(
                '<strong class="green-bg">No problem</strong> was found on the page!'
            )
        else:
            if nb_problems == 0:
                context["nb_problems_warnings"] = _(
                    '<strong class="green-bg">No problem</strong> found, and '
                )
            else:
                context["nb_problems_warnings"] = '<strong class="red-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_problems} problem</strong> found, and ",
                    "{nb_problems} problems</strong> found, and ",
                    nb_problems,
                ).format(nb_problems=nb_problems)

            if nb_warnings == 0:
                context["nb_problems_warnings"] += _(
                    '<strong class="green-bg">no warning</strong> raised'
                )
            else:
                context["nb_problems_warnings"] += '<strong class="yellow-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_warnings} warning</strong> raised",
                    "{nb_warnings} warnings</strong> raised",
                    nb_warnings,
                ).format(nb_warnings=nb_warnings)

        return context

#*******************************************************************************

class WebsiteView(DetailView):
    model = Website
    template_name = 'tools/seo.html'
    form_class = WebsiteForm

    def get_context_data(self, *args, **kwargs):
        context = super(DetailView, self).get_context_data(*args, **kwargs)

        # use credentials if provided (pass through .htaccess auth)
        if (
            settings.DJANGO_CHECK_SEO_AUTH
            and settings.DJANGO_CHECK_SEO_AUTH["user"] is not None
            and settings.DJANGO_CHECK_SEO_AUTH["pass"] is not None
        ):
            r = requests.get(
                context['object'].link,
                auth=(
                    settings.DJANGO_CHECK_SEO_AUTH["user"],
                    settings.DJANGO_CHECK_SEO_AUTH["pass"],
                ),
                headers={"Cache-Control": "no-store"},
                allow_redirects=False,
            )
            if (
                300 < r.status_code < 400
                and settings.DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS
            ):
                r = requests.get(
                    r.headers["Location"],
                    auth=(
                        settings.DJANGO_CHECK_SEO_AUTH["user"],
                        settings.DJANGO_CHECK_SEO_AUTH["pass"],
                    ),
                )
        else:
            r = requests.get(context['object'].link, headers={"Cache-Control": "no-store"})

        soup = BeautifulSoup(r.text, features="lxml")

        # populate class with data
        page_stats = site.Site(soup, context['object'].link)

        # magic happens here!
        launch_checks.launch_checks(page_stats)

        # end of magic, get collected problems/warnings/success and put them inside the context
        (context["problems"], context["warnings"], context["success"]) = (
            page_stats.problems,
            page_stats.warnings,
            page_stats.success,
        )

        context["settings"] = json.dumps(settings.DJANGO_CHECK_SEO_SETTINGS, indent=4)
        context["html"] = page_stats.content
        context["text"] = page_stats.content_text
        context["keywords"] = page_stats.keywords

        nb_problems = len(context["problems"])
        nb_warnings = len(context["warnings"])

        # define some fancy-looking text here because it is wayyy to dirty with all the {% trans %} in template
        if nb_problems == 0 and nb_warnings == 0:
            context["nb_problems_warnings"] = _(
                '<strong class="green-bg">No problem</strong> was found on the page!'
            )
        else:
            if nb_problems == 0:
                context["nb_problems_warnings"] = _(
                    '<strong class="green-bg">No problem</strong> found, and '
                )
            else:
                context["nb_problems_warnings"] = '<strong class="red-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_problems} problem</strong> found, and ",
                    "{nb_problems} problems</strong> found, and ",
                    nb_problems,
                ).format(nb_problems=nb_problems)

            if nb_warnings == 0:
                context["nb_problems_warnings"] += _(
                    '<strong class="green-bg">no warning</strong> raised'
                )
            else:
                context["nb_problems_warnings"] += '<strong class="yellow-bg">'
                context["nb_problems_warnings"] += ngettext(
                    "{nb_warnings} warning</strong> raised",
                    "{nb_warnings} warnings</strong> raised",
                    nb_warnings,
                ).format(nb_warnings=nb_warnings)

        return context
