from django2use.helpers import *

from django2use.osint.models import *
from django2use.osint.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

#*******************************************************************************

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

################################################################################

class DefaultView(TemplateView):
    template_name = 'home.html'

    @method_decorator(ensure_csrf_cookie)
    def get(self, request, *args, **kwargs):
        return super(DefaultView, self).get(request, *args, **kwargs)

#*******************************************************************************

class DomainListView(ListView):
    model = Domain
    template_name = 'domain/list.html'
    form_class = DomainForm

class DomainReadView(DetailView):
    model = Domain
    template_name = 'domain/item.html'
    form_class = DomainForm

class DomainMakeView(CreateView):
    model = Domain
    template_name = 'domain/make.html'
    fields = ['owner','alias','start','finis','price']

class DomainEditView(UpdateView):
    model = Domain
    template_name = 'domain/edit.html'
    fields = ['alias','start','finis','price']

class DomainToolView(DetailView):
    model = Domain
    template_name = 'domain/tool.html'
    form_class = DomainForm

class DomainKillView(DeleteView):
    model = Domain
    success_url = reverse_lazy('domain_list')

#*********************************************************************

class MachineListView(ListView):
    model = Machine
    template_name = 'machine/list.html'
    form_class = MachineForm

class MachineReadView(DetailView):
    model = Machine
    template_name = 'machine/item.html'
    form_class = MachineForm

class MachineMakeView(CreateView):
    model = Machine
    template_name = 'machine/make.html'
    fields = ['owner','alias','mac_addr','ip4_addr','ip6_addr','provider','hostname']

class MachineEditView(UpdateView):
    model = Machine
    template_name = 'machine/edit.html'
    fields = ['alias','ip4_addr','ip6_addr','provider','hostname']

class MachineToolView(DetailView):
    model = Machine
    template_name = 'machine/tool.html'
    form_class = MachineForm

class MachineKillView(DeleteView):
    model = Machine
    success_url = reverse_lazy('machine_list')
