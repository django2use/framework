from django2use.helpers import *

from django2use.osint.models import *
from django2use.osint.forms import *
from django2use.osint.tasks import *

################################################################################

class ToolbeltListView(ListView):
    model = Toolbelt
    template_name = 'toolbelt/list.html'
    form_class = ToolbeltForm

class ToolbeltReadView(DetailView):
    model = Toolbelt
    template_name = 'toolbelt/item.html'
    form_class = ToolbeltForm

class ToolbeltMakeView(CreateView):
    model = Toolbelt
    fields = [
        'alias','label','helps','state',
        'website','flavour','command','install',
    ]
    template_name = 'toolbelt/make.html'

class ToolbeltEditView(UpdateView):
    model = Toolbelt
    fields = [
        'alias','label','helps','state',
        'website','flavour','command','install',
    ]
    template_name = 'toolbelt/edit.html'

class ToolbeltKillView(DeleteView):
    model = Toolbelt
    success_url = reverse_lazy('toolbelt_list')

#*******************************************************************************

class ToolbeltScanView(FormView):
    template_name = 'toolbelt/scan.html'
    form_class = ToolbeltScan
    success_url = reverse_lazy('toolbelt_list')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_email()
        return super().form_valid(form)

################################################################################

class ConfigurationListView(ListView):
    model = Configuration
    template_name = 'configuration/list.html'
    form_class = ConfigurationForm

class ConfigurationReadView(DetailView):
    model = Configuration
    template_name = 'configuration/item.html'
    form_class = ConfigurationForm

class ConfigurationMakeView(CreateView):
    model = Configuration
    fields = ['tool','name','flag','meta','data']
    template_name = 'configuration/make.html'

class ConfigurationEditView(UpdateView):
    model = Configuration
    fields = ['tool','name','flag','meta','data']
    template_name = 'configuration/edit.html'

class ConfigurationKillView(DeleteView):
    model = Configuration
    success_url = reverse_lazy('configuration_list')
