from django2use.route.Common import *

from django.contrib import admin

#from django2use.osint.views import WorkspaceScanView

from django2use.osint import views

webpatterns = [
    url(r'^$', views.HOME.MetricView.as_view(), name='home'),

    path('toolbelt/',                views.TOOL.ToolbeltListView.as_view(), name='toolbelt_list'),
    path('toolbelt/<int:pk>/',       views.TOOL.ToolbeltReadView.as_view(), name='toolbelt_read'),
    path('toolbelt/add/',            views.TOOL.ToolbeltMakeView.as_view(), name='toolbelt_make'),
    path('toolbelt/<int:pk>/edit',   views.TOOL.ToolbeltEditView.as_view(), name='toolbelt_edit'),
    path('toolbelt/<int:pk>/delete', views.TOOL.ToolbeltKillView.as_view(), name='toolbelt_kill'),

    path('toolbelt/<int:pk>/check',  views.TOOL.ToolbeltScanView.as_view(), name='toolbelt_test'),

    #path('configuration/',                views.TOOL.ConfigurationListView.as_view(), name='configuration_list'),
    #path('configuration/<int:pk>/',       views.TOOL.ConfigurationReadView.as_view(), name='configuration_read'),
    #path('configuration/add/',            views.TOOL.ConfigurationMakeView.as_view(), name='configuration_make'),
    #path('configuration/<int:pk>/edit',   views.TOOL.ConfigurationEditView.as_view(), name='configuration_edit'),
    #path('configuration/<int:pk>/delete', views.TOOL.ConfigurationKillView.as_view(), name='configuration_kill'),

    path("seo/generic/<str:pk>",       views.SERP.GenericView.as_view(), name="serp_generic"),
    path("seo/domain/<int:pk>",        views.SERP.DomainView.as_view(),  name="serp_domain"),
    path("seo/website/<int:pk>",       views.SERP.WebsiteView.as_view(), name="serp_website"),
]

if False:
    from .views import index, engine, config, wordly, notifier

    urlpatterns += [
        path(
            '',
            index,
            name='scan_engine_index'),
        #---------------------------------------------------------------------------
        path(
            'engine/add/',
            engine.add_engine,
            name='add_engine'),
        path(
            'engine/delete/<int:id>',
            engine.delete_engine,
            name='delete_engine_url'),
        path(
            'engine/update/<int:id>',
            engine.update_engine,
            name='update_engine'),
        #---------------------------------------------------------------------------
        path(
            'wordlist/',
            wordly.wordlist_list,
            name='wordlist_list'),
        path(
            'wordlist/add/',
            wordly.add_wordlist,
            name='add_wordlist'),
        path(
            'wordlist/delete/<int:id>',
            wordly.delete_wordlist,
            name='delete_wordlist'),
        #---------------------------------------------------------------------------
        path(
            'configuration/',
            config.configuration_list,
            name='configuration_list'),
        path(
            'configuration/add/',
            config.add_configuration,
            name='add_configuration'),
        path(
            'configuration/update/<int:id>',
            config.update_configuration,
            name='update_configuration'),
        path(
            'configuration/delete/<int:id>',
            config.delete_configuration,
            name='delete_configuration'),
        #---------------------------------------------------------------------------
        path(
            'notification/',
            notifier.index,
            name='notification_index'),
        path(
            'notification/change/<int:id>',
            notifier.change_notif_status,
            name='change_notif_status'),
        path(
            'notification/add/',
            notifier.add_notification_hook,
            name='add_notification_hook'),
        path(
            'notification/delete/<int:id>',
            notifier.delete_hook,
            name='delete_hook'),
    ]

urlpatterns = pattern2use(webpatterns)
