import validators

from django.core.management.base import BaseCommand, CommandError

from datetime import datetime
from ipaddress import ip_address

from targets.models import *
from scanner.models import *
from results.models import *

class Command(BaseCommand):
    help = 'Install scanning tools defined in the application.'

    def add_arguments(self, parser):
        #parser.add_argument('space', type=str, help='Indicates the number of users to be created')

        #parser.add_argument('targets', nargs='+', type=str)

        parser.add_argument(
            '--all',
            action='store_true',
            help="Install all tools (not only activated ones).",
        )

        parser.add_argument(
            '--dry',
            action='store_true',
            help="Don't actually execute anything",
        )

        #parser.add_argument('-s', '--space', type=str, help='Define the workspace to use')

    def handle(self, *args, **options):
        print("*) Installing scanning tools :")

        qry = Toolbelt.objects.order_by('alias')

        if not options['all']:
            qry = qry.filter(state=True)

        for tool in qry:
            if tool.missing:
                print("\t-> Installing program '%(label)s' at : %(bpath)s" % tool.__dict__)
            else:
                print("\t=> Program '%(label)s' installed already." % tool.__dict__)

            tool.prepare(options['dry']) # (**options)

        print("#) Installation complete.")

        #self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % space))
