import validators

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

from datetime import datetime
from ipaddress import ip_address

from targets.models import *
from scanner.models import *
from results.models import *

class Command(BaseCommand):
    help = 'Start a new scan from the command line.'

    def add_arguments(self, parser):
        #parser.add_argument('space', type=str, help='Indicates the number of users to be created')

        parser.add_argument('targets', nargs='+', type=str)

        parser.add_argument('-s', '--space', type=str, help='Define the workspace to use')

        parser.add_argument(
            '--check',
            action='store_true',
            help='Perform a Vulnerability Scan on the Results',
        )

        parser.add_argument('-c', '--comment', type=str, help='Add comments to the scan history')

    def check_adr(self, value):
        try:
            ip = ip_address(value)
        except:
            return False

        return True

    def check_dns(self, value): return validators.domain(value)
    def check_url(self, value): return (value)

    def handle(self, *args, **options):
        try:
            work = Workspace.objects.get(alias=options['space'])
        except Workspace.DoesNotExist:
            raise CommandError('Workspace "%s" does not exist' % space)

        maps = {}

        print("*) Parsing targets :")
        print("")

        for item in options['targets']:
            task = ScanHistory(
                wspace = work,
                target = item,
            )

            print("\t-> %(target)s (%(id)d)" % task.__dict__)

        #for host in maps['dns']['item']:
        #    task.hostnames.append(host)
        #
        #for addr in maps['adr']['item']:
        #    task.addresses.append(addr)

        task.save()

        print("")

        call_command('process',task.id)
