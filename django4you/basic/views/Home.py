from django2use.helpers import *

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

class DefaultView(TemplateView):
    template_name = 'home.html'

    @method_decorator(ensure_csrf_cookie)
    def get(self, request, *args, **kwargs):
        return super(DefaultView, self).get(request, *args, **kwargs)

#*******************************************************************************

class SessionView(TemplateView):
    template_name = 'home_session.html'

    @method_decorator(ensure_csrf_cookie)
    def get(self, request, *args, **kwargs):
        return super(SessionView, self).get(request, *args, **kwargs)

################################################################################

class TokenView(TemplateView):
    template_name = 'home_token.html'

class JWTView(TemplateView):
    template_name = 'home_jwt.html'

class KnoxView(TemplateView):
    template_name = 'home_knox.html'
