from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class LandingView(TemplateView):
    template_name = 'cpanel/landing.html'

################################################################################

class AccountView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'oauth2/home.html'
    form_class = AccountForm

#*******************************************************************************

class ProfileView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'cpanel/profile.html'
    form_class = ProfileForm

#*******************************************************************************

class SettingView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'cpanel/setting.html'
    form_class = SettingForm

################################################################################

class AgendaView(TemplateView):
    template_name = 'cpanel/agenda.html'

#*******************************************************************************

class BillingView(TemplateView):
    template_name = 'cpanel/billing.html'

#*******************************************************************************

class ContactView(TemplateView):
    template_name = 'cpanel/contact.html'

#*******************************************************************************

class MailboxView(TemplateView):
    template_name = 'cpanel/mailbox.html'

#*******************************************************************************

class UpgradeView(TemplateView):
    template_name = 'cpanel/upgrade.html'
