from django.conf.urls import include, url

from django2use.basic import views

urlpatterns = [
    url(r'^logout/$', views.User.LogoutSessionView.as_view(), name='logout_session'),

    url(r'^user/session/', views.User.SessionDetailView.as_view(), name="current_user_session"),
    url(r'^user/token/', views.User.TokenDetailView.as_view(), name="current_user_token"),
    #url(r'^user/jwt/', views.User.JWTDetailView.as_view(), name="current_user_jwt"),

    url(r'^$', views.Page.AccountView.as_view(), name="home"),
]
