from django.conf.urls import include, url

from django2use.basic import views

urlpatterns = [
    url(r'^.*\.html', views.Page.gentella_html, name='gentella'),
    #url(r'^$', views.Home.DefaultView.as_view(), name='home'),

    url(r'^dashboard/session/$', views.Home.SessionView.as_view(), name='home_session'),
    url(r'^dashboard/token/$', views.Home.TokenView.as_view(), name='home_token'),
    url(r'^dashboard/jwt/$', views.Home.JWTView.as_view(), name='home_jwt'),

    url(r'^$', views.Page.LandingView.as_view(), name='home'),
    url(r'^agendas$', views.Page.AgendaView.as_view(), name='agendas'),
    url(r'^billing$', views.Page.BillingView.as_view(), name='billing'),
    url(r'^contact$', views.Page.ContactView.as_view(), name='contact'),
    url(r'^mailbox$', views.Page.MailboxView.as_view(), name='mailbox'),
    url(r'^profile$', views.Page.ProfileView.as_view(), name='profile'),
    url(r'^setting$', views.Page.SettingView.as_view(), name='setting'),
    url(r'^upgrade$', views.Page.UpgradeView.as_view(), name='upgrade'),
]

