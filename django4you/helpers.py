from .library import *

from django.views.decorators.csrf import csrf_exempt

################################################################################

from rest_framework import routers

APIv1 = routers.DefaultRouter()

#*******************************************************************************

from django2use.basic import objects as R_basic

APIv1.register(r'basic/language', R_basic.LanguageViewSet)
APIv1.register(r'basic/organism', R_basic.OrganismViewSet)
APIv1.register(r'basic/identity', R_basic.IdentityViewSet)
APIv1.register(r'basic/group',    R_basic.GroupViewSet)

from django2use.panel import objects as R_panel

################################################################################

from django.core.management.base import BaseCommand

#*******************************************************************************

class Commandline(BaseCommand): # ,DatenMixin,PosixMixin,WiresMixin):
    pass

################################################################################

#from django.contrib.sites.models import Site
from django.utils.translation import gettext as _
from django.utils.translation import ngettext

#*******************************************************************************

from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView

from django.contrib import admin

from django.contrib.auth import logout, get_user_model, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.signals import user_logged_out, user_logged_in

from rest_framework import status

from rest_framework import generics
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import status

from rest_framework.decorators import api_view as drf_view, action as drf_action
from rest_framework.permissions import IsAdminUser, AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
