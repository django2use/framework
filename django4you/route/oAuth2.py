from django.urls import path, include
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from django2use import settings

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

urlpatterns = [
    path('',    include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('',    include('social_django.urls', namespace='social')),

    path('',    include('django2use.basic.urls.Auth')),

    url(r'^login/session/', include('rest_social_auth.urls_session')),
    url(r'^login/token/', include('rest_social_auth.urls_token')),
    #url(r'^login/jwt/', include('rest_social_auth.urls_simplejwt')),

    url(r'^jwt/token', obtain_jwt_token),
    url(r'^jwt/fresh', refresh_jwt_token),
    url(r'^jwt/verif', verify_jwt_token),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

