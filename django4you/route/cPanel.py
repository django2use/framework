from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from django2use import settings

from django.contrib import admin

from graphene_django.views import GraphQLView

urlpatterns = [
    path('',          include('django2use.basic.urls.Page')),

    path('graph/ql',  csrf_exempt(GraphQLView.as_view(graphiql=False))),
    path('graph/iql', csrf_exempt(GraphQLView.as_view(graphiql=True))),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
