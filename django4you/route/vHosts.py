from django_hosts import patterns, host

from django2use.settings import APP_FQDN

host_patterns = patterns('',
    host(r'manage.%s' % APP_FQDN, 'django2use.route.Manager', name='manage'),
    host(r'apis.%s' % APP_FQDN,   'django2use.route.APIs',    name='apis'),
    host(r'tenant.%s' % APP_FQDN, 'django2use.route.oAuth2',  name='tenant'),
    host(r'cpanel.%s' % APP_FQDN, 'django2use.route.cPanel',  name='cpanel'),
    host(r'browse.%s' % APP_FQDN, 'django2use.route.Browse',  name='browse'),
    host(r'editor.%s' % APP_FQDN, 'django2use.route.Editor',  name='editor'),
    host(r'studio.%s' % APP_FQDN, 'django2use.route.Studio',  name='studio'),
)
