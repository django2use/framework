from django2use.helpers import *

from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from django2use import settings

from django.contrib import admin

from graphene_django.views import GraphQLView
from rest_framework.schemas import get_schema_view
from graph_wrap.django_rest_framework.graphql_view import graphql_view

urlpatterns = [
    path('admin/',    admin.site.urls),
    path('queue/',    include('django2use.queue.mains.urls')),
    path('clock/',    include('django2use.queue.defer.urls')),

    path('oauth/',    include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('oauth/',    include('django2use.basic.urls.Auth')),

    path('apis/',     include('django2use.route.APIs')),

    path('graph/ql',  csrf_exempt(GraphQLView.as_view(graphiql=False))),
    path('graph/iql', csrf_exempt(GraphQLView.as_view(graphiql=True))),

    path('',          include('django2use.basic.urls.Page')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
