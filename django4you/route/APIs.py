from django2use.helpers import *

from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from django2use import settings

from django.contrib import admin

from graphene_django.views import GraphQLView
from rest_framework.schemas import get_schema_view
from graph_wrap.django_rest_framework.graphql_view import graphql_view

from django2use.panel.views.NAME import dynamic_dns_read, dynamic_dns_update

urlpatterns = [
    path('',       include('rest_framework.urls', namespace='rest_framework')),
    path('',       include(APIv1.urls)),

    path('redoc/', TemplateView.as_view(
        template_name='redoc.html',
        extra_context={'schema_url':'openapi'}
    ), name='redoc'),
    path('swagger/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi'}
    ), name='swagger'),
    path('openapi', get_schema_view(
        title=settings.APP_NAME,
        description=settings.APP_HELP,
        version=settings.APP_CODE,
    ), name='openapi'),
    path('graphql', view=graphql_view, name='schema-graphql'),

    #path('graph/ql',  csrf_exempt(GraphQLView.as_view(graphiql=False))),
    #path('graph/iql', csrf_exempt(GraphQLView.as_view(graphiql=True))),

    path('nic/read/<str:domain>/', dynamic_dns_read),
    path('nic/update/<str:domain>/', dynamic_dns_update),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
