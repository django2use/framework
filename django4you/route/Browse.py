from django.urls import path, include
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from django2use import settings

from django.contrib import admin

from django2use.panel import views

urlpatterns = [
    url(r'^$',        views.FILE.ExploreView.as_view(), name='explore'),
    url(r'^creator$', views.FILE.CreatorView.as_view(), name='creator'),
    url(r'^gallery$', views.FILE.GalleryView.as_view(), name='gallery'),
    url(r'^tabular$', views.FILE.TabularView.as_view(), name='tabular'),
    url(r'^uploads$', views.FILE.UploadsView.as_view(), name='uploads'),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
