from . import Default
from . import Generic

from . import Manager
from . import oAuth2
from . import APIs

from . import cPanel

from . import vHosts
