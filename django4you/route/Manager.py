from django.contrib import admin
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

import debug_toolbar

urlpatterns = [
    path('clock/',    include('django2use.queue.defer.urls')),
    path('__debug__', include(debug_toolbar.urls), name='djdt'),
    path('queue/',    include('django2use.queue.mains.urls')),
    path('',          admin.site.urls),
    path('',          include('admin_tools.urls')),
]
