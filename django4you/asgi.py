"""
ASGI config for django2use project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django2use.settings')

from django2use.basic import routing as R_basic

application = ProtocolTypeRouter({
    "websocket": AuthMiddlewareStack(
        URLRouter(
            R_basic.websocket_urlpatterns,
        )
    ),
    "http": get_asgi_application(),
})
