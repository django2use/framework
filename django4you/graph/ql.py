import graphene

import django2use.basic.graphql as basic_ql
import django2use.panel.graphql as panel_ql

#*********************************************************************

class Query(
    basic_ql.Query,
graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

######################################################################

schema = graphene.Schema(query=Query)

mapped = {
    None:    schema,
    'basic': basic_ql.schema,
    'panel': panel_ql.schema,
}
