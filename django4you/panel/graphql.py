import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Identity, Group

######################################################################

class DomainType(DjangoObjectType):
    class Meta:
        model = Domain
        filter_fields = [
            'owner', 'alias',
            'start', 'finis',
            'price',
        ]
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class MachineType(DjangoObjectType):
    class Meta:
        model = Machine
        filter_fields = [
            'owner', 'alias',
            'hw_addr', 'ipv4addr', 'ipv6addr',
            'username', 'password',
        ]
        interfaces = (graphene.relay.Node, )

##########################################################################

class HostingType(DjangoObjectType):
    class Meta:
        model = Hosting
        filter_fields = [
            'owner', 'alias',
            'target', 'pseudo', 'passwd', 'domain',
        ]
        interfaces = (graphene.relay.Node, )

#*********************************************************************

class WebsiteType(DjangoObjectType):
    class Meta:
        model = Website
        filter_fields = [
            'owner', 'alias', 'frame', 'where',
            'target', 'pseudo', 'passwd', #'domain',
        ]
        interfaces = (graphene.relay.Node, )

##########################################################################

class Query(graphene.ObjectType):
    one_domain = graphene.relay.Node.Field(LanguageType)
    all_domain = DjangoFilterConnectionField(LanguageType)

    one_organism = graphene.relay.Node.Field(OrganismType)
    all_organism = DjangoFilterConnectionField(OrganismType)

    one_identity = graphene.relay.Node.Field(IdentityType)
    all_identity = DjangoFilterConnectionField(IdentityType)

#*********************************************************************

schema = graphene.Schema(query=Query)
