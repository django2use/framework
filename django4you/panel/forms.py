from django import forms

#*********************************************************************

from .models import *

######################################################################

class DomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        exclude = ['owner']

#*********************************************************************

class MachineForm(forms.ModelForm):
    class Meta:
        model = Machine
        exclude = ['owner']

######################################################################

class HostingForm(forms.ModelForm):
    class Meta:
        model = Hosting
        exclude = ['owner']

#*********************************************************************

class WebsiteForm(forms.ModelForm):
    class Meta:
        model = Website
        exclude = ['owner']

