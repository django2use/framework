from django.conf.urls import include, path, url

from django.contrib.admin.views.decorators import staff_member_required

from django2use.basic import views

urlpatterns = [
    url(r'^$', views.DefaultView.as_view(), name='home'),
    #url(r'^$', views.LandingView.as_view(), name='home'),
    url(r'^.*\.html',              views.gentella_html, name='gentella'),

    path('domain/',                    views.DomainListView.as_view(),  name='domain_list'),
    path('domain/<int:pk>/',           views.DomainReadView.as_view(),  name='domain_read'),
    path('domain/add/',                views.DomainMakeView.as_view(),  name='domain_make'),
    path('domain/<int:pk>/edit',       views.DomainEditView.as_view(),  name='domain_edit'),
    path('domain/<int:pk>/tools',      views.DomainToolView.as_view(),  name='domain_tool'),
    path('domain/<int:pk>/delete',     views.DomainKillView.as_view(),  name='domain_kill'),

    path('machine/',                   views.MachineListView.as_view(), name='machine_list'),
    path('machine/<int:pk>/',          views.MachineReadView.as_view(), name='machine_read'),
    path('machine/add/',               views.MachineMakeView.as_view(), name='machine_make'),
    path('machine/<int:pk>/edit',      views.MachineEditView.as_view(), name='machine_edit'),
    path('machine/<int:pk>/tools',     views.MachineToolView.as_view(), name='machine_tool'),
    path('machine/<int:pk>/delete',    views.MachineKillView.as_view(), name='machine_kill'),

    path('hosting/',                   views.HostingListView.as_view(), name='hosting_list'),
    path('hosting/<int:pk>/',          views.HostingReadView.as_view(), name='hosting_read'),
    path('hosting/add/',               views.HostingMakeView.as_view(), name='hosting_make'),
    path('hosting/<int:pk>/edit',      views.HostingEditView.as_view(), name='hosting_edit'),
    #path('hosting/<int:pk>/tools',     views.HostingToolView.as_view(), name='hosting_tool'),
    path('hosting/<int:pk>/delete',    views.HostingKillView.as_view(), name='hosting_kill'),

    path('website/',                   views.WebsiteListView.as_view(), name='website_list'),
    path('website/<int:pk>/',          views.WebsiteReadView.as_view(), name='website_read'),
    path('website/add/',               views.WebsiteMakeView.as_view(), name='website_make'),
    path('website/<int:pk>/edit',      views.WebsiteEditView.as_view(), name='website_edit'),
    #path('website/<int:pk>/tools',     views.WebsiteToolView.as_view(), name='website_tool'),
    path('website/<int:pk>/delete',    views.WebsiteKillView.as_view(), name='website_kill'),

    path("domain/<int:pk>/seo",        views.SEO.DomainView.as_view(),  name="domain_tool")
    path("website/<int:pk>/seo",       views.SEO.WebsiteView.as_view(), name="website_tool")

    #path("seo/domain/<int:pk>",        staff_member_required(views.SEO.DomainView.as_view()), name="tools_seo_dns")
]

