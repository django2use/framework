from rest_framework import serializers

from .models import *

######################################################################

class DomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'start', 'finis',
            'price',
        ]
        model = Domain

#*********************************************************************

class MachineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'hw_addr', 'ipv4addr', 'ipv6addr',
            'username', 'password',
        ]
        model = Machine

######################################################################

class HostingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias',
            'target', 'pseudo', 'passwd', 'domain',
        ]
        model = Hosting

#*********************************************************************

class WebsiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = [
            'owner', 'alias', 'frame', 'where',
            'target', 'pseudo', 'passwd', #'domain',
        ]
        model = Website
