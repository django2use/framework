from django.contrib import admin

from .models import *

######################################################################

class DomainAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','start','finis','price']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Domain, DomainAdmin)

#*********************************************************************

class MachineAdmin(admin.ModelAdmin):
    list_display = ('owner', 'alias', 'ip4_addr', 'ip6_addr', 'provider', 'hostname', 'last_time',)
    ordering = ('owner__username', 'last_time', 'provider')
    #list_filter = ('provider', 'last_time')

admin.site.register(Machine, MachineAdmin)

######################################################################

class HostingAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','target','pseudo','passwd']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Hosting, HostingAdmin)

#*********************************************************************

class WebsiteAdmin(admin.ModelAdmin):
    list_display  = ['owner','alias','frame','where','target','pseudo','passwd']
    #list_filter   = ['is_superuser']
    #list_editable = ['is_superuser']

admin.site.register(Website, WebsiteAdmin)
