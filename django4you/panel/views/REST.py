from django2use.helpers import *

from django2use.panel.models import *
from django2use.panel.forms import *

#*******************************************************************************

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.template import loader
from django.http import HttpResponse

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import SimpleJWTAuthMixin, KnoxAuthMixin

class DefaultView(TemplateView):
    template_name = 'home.html'

    @method_decorator(ensure_csrf_cookie)
    def get(self, request, *args, **kwargs):
        return super(DefaultView, self).get(request, *args, **kwargs)

################################################################################

class DomainListView(ListView):
    model = Domain
    template_name = 'domain/list.html'
    form_class = DomainForm

class DomainReadView(DetailView):
    model = Domain
    template_name = 'domain/item.html'
    form_class = DomainForm

class DomainMakeView(CreateView):
    model = Domain
    template_name = 'domain/make.html'
    fields = ['space','alias','label','engin','token','check']

class DomainEditView(UpdateView):
    model = Domain
    template_name = 'domain/edit.html'
    fields = ['space','alias','label','engin','token','check']

class DomainToolView(DetailView):
    model = Domain
    template_name = 'domain/tool.html'
    form_class = DomainForm

class DomainKillView(DeleteView):
    model = Domain
    success_url = reverse_lazy('domain_list')

#*********************************************************************

class MachineListView(ListView):
    model = Machine
    template_name = 'machine/list.html'
    form_class = MachineForm

class MachineReadView(DetailView):
    model = Machine
    template_name = 'machine/item.html'
    form_class = MachineForm

class MachineMakeView(CreateView):
    model = Machine
    template_name = 'machine/make.html'
    fields = ['space','alias','label','engin','token','check']

class MachineEditView(UpdateView):
    model = Machine
    template_name = 'machine/edit.html'
    fields = ['space','alias','label','engin','token','check']

class MachineToolView(DetailView):
    model = Machine
    template_name = 'machine/tool.html'
    form_class = MachineForm

class MachineKillView(DeleteView):
    model = Machine
    success_url = reverse_lazy('machine_list')

################################################################################

class HostingListView(ListView):
    model = Hosting
    template_name = 'hosting/list.html'
    form_class = HostingForm

class HostingReadView(DetailView):
    model = Hosting
    template_name = 'hosting/item.html'
    form_class = HostingForm

class HostingMakeView(CreateView):
    model = Hosting
    template_name = 'hosting/make.html'
    fields = ['space','alias','label','engin','token','check']

class HostingEditView(UpdateView):
    model = Hosting
    template_name = 'hosting/edit.html'
    fields = ['space','alias','label','engin','token','check']

class HostingToolView(DetailView):
    model = Hosting
    template_name = 'hosting/tool.html'
    form_class = HostingForm

class HostingKillView(DeleteView):
    model = Hosting
    success_url = reverse_lazy('hosting_list')

#*********************************************************************

class WebsiteListView(ListView):
    model = Website
    template_name = 'website/list.html'
    form_class = WebsiteForm

class WebsiteReadView(DetailView):
    model = Website
    template_name = 'website/item.html'
    form_class = WebsiteForm

class WebsiteMakeView(CreateView):
    model = Website
    template_name = 'website/make.html'
    fields = ['space','alias','label','engin','token','check']

class WebsiteEditView(UpdateView):
    model = Website
    template_name = 'website/edit.html'
    fields = ['space','alias','label','engin','token','check']

class WebsiteToolView(DetailView):
    model = Website
    template_name = 'website/tool.html'
    form_class = WebsiteForm

class WebsiteKillView(DeleteView):
    model = Website
    success_url = reverse_lazy('website_list')
