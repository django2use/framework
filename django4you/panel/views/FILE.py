from django2use.helpers import *

#*******************************************************************************

from django2use.basic.forms import *

################################################################################

def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

#*******************************************************************************

class ExploreView(TemplateView):
    template_name = 'studio/explore.html'

################################################################################

class CreatorView(TemplateView):
    template_name = 'studio/creator.html'

#*******************************************************************************

class GalleryView(TemplateView):
    template_name = 'studio/gallery.html'

#*******************************************************************************

class TabularView(TemplateView):
    template_name = 'studio/tabular.html'

#*******************************************************************************

class UploadsView(TemplateView):
    template_name = 'studio/uploads.html'

################################################################################

class ProfileView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'cpanel/profile.html'
    form_class = SettingForm

#*******************************************************************************

class SettingView(UpdateView):
    def get_object(self, *args, **kwargs):
        current = self.request.user

        return current

    model = Identity
    template_name = 'cpanel/setting.html'
    form_class = AccountForm
    #exclude = ['username','password']
