from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class DeferConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django2use.queue.defer'
    verbose_name = _('Redis-Queue')
