from rest_framework_httpsignature.authentication import SignatureAuthentication

class MyAuthentication(SignatureAuthentication):
    API_KEY_HEADER = 'X-Api-Key'
    # rsa-sha1, rsa-sha256, rsa-sha512, hmac-sha1, hmac-sha256, hmac-sha512
    ALGORYTHM = 'rsa-256'

    # A method to fetch (User instance, user_secret_string) from the
    # consumer key ID, or None in case it is not found.
    def fetch_user_data(self, api_key):
        # ...
        # example implementation:
        try:
            user = User.objects.get(api_key=api_key)
            return (user, user.secret)
        except User.DoesNotExist:
            return None
