from pathlib import Path
import os,sys,re,datetime
from datetime import date, time, timedelta

import dj_database_url
from decouple import config as decoupled

import os,sys,time,click,math,codecs
from tqdm import tqdm
#from urlparse import *
import simplejson as json,yaml
from uuid import uuid4
import socket
from contextlib import closing
import requests
from bs4 import BeautifulSoup
import string, random

#*******************************************************************************

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

rpath = lambda *x: os.path.join(BASE_DIR, *x)
bpath = lambda *x: os.path.join(BASE_DIR, 'django2use', *x)
fpath = lambda *x: os.path.join(BASE_DIR, 'thevault', *x)

rlink = lambda *x: urljoin(MAIN_LINK,'/'.join(x))

#*******************************************************************************

from django import forms
from django.db import models

################################################################################

from django_better_admin_arrayfield.models.fields import ArrayField

#*******************************************************************************

from phonenumber_field.modelfields import PhoneNumberField
