from django2use.library import *

SECRET_KEY = decoupled('APP_SECRET', default='5i830%k0$u*+@0290eq&lb!c%h3cxknj01ygyck-@el-5__y4y')

APP_NAME = decoupled('APP_NAME')
APP_HELP = decoupled('APP_HELP')

APP_CODE = decoupled('APP_CODE', default="0.0.1")
APP_FQDN = decoupled('APP_FQDN', default=None)

DEFAULT_COUNTRY = 'ma'
DEFAULT_LANGAGE = 'en'

PHONENUMBER_DEFAULT_REGION = decoupled('APP_PAYS', default=DEFAULT_COUNTRY).upper()

LANGUAGE_CODE = decoupled('APP_LANG', default=DEFAULT_LANGAGE+'-'+PHONENUMBER_DEFAULT_REGION)
TIME_ZONE     = decoupled('APP_TIME', default='UTC')

DEBUG = True

USE_I18N = True
USE_TZ = True

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]

SESSION_COOKIE_DOMAIN = "."+APP_FQDN

if DEBUG:
    import os  # only if you haven't already imported this
    import socket  # only if you haven't already imported this
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS = [ip[:-1] + '1' for ip in ips] + ['127.0.0.1', '10.0.2.2']

ALLOWED_HOSTS = ['*']

LOGIN_URL='/admin/login/'

AUTH_USER_MODEL = 'basic.Identity'
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

MEDIA_ROOT = fpath('media')
STATIC_ROOT = fpath('serve')

STATICFILES_DIRS = (
    fpath('asset'),
)

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

INSTALLED_APPS = [
    'channels',
        'admin_tools',
        'admin_tools.theming',
        'admin_tools.menu',
        'admin_tools.dashboard',
    #'suit',
    #'suit_rq',
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    'mathfilters',
        'django_better_admin_arrayfield',
    'corsheaders',
        'django_hosts',
        'social_django',
        'import_export',
    'debug_toolbar',
        'django2use.basic',
    'oauth2_provider',
        'rest_framework',
        'rest_framework.authtoken',
        'rest_framework_httpsignature',
        'rest_social_auth',
        'rest_framework_swagger',
    'django2use.queue.mains',
    'django2use.queue.defer',
    'django2use.queue.clock',
        'phonenumber_field',
        'django_filters',
        'graphene_django',
    'django2use.panel',
    ##'mptt',
    ##'elfinder',
]

GRAPHENE = {
    'SCHEMA': 'django2use.graph.ql.schema',
    'SCHEMA_OUTPUT': bpath('graph', 'schema.graphql')
}

ROOT_HOSTCONF = 'django2use.route.vHosts'
ASGI_APPLICATION = 'django2use.asgi.application'
WSGI_APPLICATION = 'django2use.wsgi.application'
ROOT_URLCONF = 'django2use.route.Generic'

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': rpath('local.sqlite'),
    },
    'pgsql': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django2use',
        'USER': 'django2use',
        'PASSWORD': 'django2use',
        'HOST': 'localhost',
        'PORT': 5432,
    },
}

if 'DATABASE_URL' in os.environ:
    DATABASES['cloud'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

if not os.path.exists(DATABASES['local']['NAME']):
    os.system('touch %(NAME)s' % DATABASES['local'])

DATABASES['default'] = DATABASES.get('cloud', DATABASES['pgsql'])

RQ_QUEUES = {
    'default': {
        'URL': decoupled('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 360,
    },
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            fpath('theme'),
            fpath('views'),
            fpath('quote'),
            fpath('pages'),
        ],
        #'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'django.template.context_processors.request',

                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',

                'django2use.template.conf_general',
                'django2use.template.user_avatar',
                'django2use.template.suit_menus',
            ],
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader']),
            ],
        },
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'rest_social_auth': {
            'handlers': ['console', ],
            'level': "DEBUG",
        },
    }
}

MIDDLEWARE = [
        'django_hosts.middleware.HostsRequestMiddleware',
    'django.middleware.security.SecurityMiddleware',
        'corsheaders.middleware.CorsMiddleware',
        'whitenoise.middleware.WhiteNoiseMiddleware',
        'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
        "debug_toolbar.middleware.DebugToolbarMiddleware",
    'django_hosts.middleware.HostsResponseMiddleware',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


SOCIAL_AUTH_PIPELINE = (
    #'specs.pipeline.auto_logout',  # custom action
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    #'specs.pipeline.check_for_email',  # custom action
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    #'specs.pipeline.save_avatar',  # custom action
)

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

#SOCIAL_AUTH_POSTGRES_JSONFIELD = True  # Before
#SOCIAL_AUTH_JSONFIELD_ENABLED = True  # After

AUTHENTICATION_BACKENDS = (
    'oauth2_provider.backends.OAuth2Backend',
    'social_core.backends.open_id.OpenIdAuth',
        'social_core.backends.google.GoogleOAuth2',
        #'social_core.backends.yahoo.YahooOpenId',
        'social_core.backends.facebook.FacebookOAuth2',
        'social_core.backends.twitter.TwitterOAuth',  # OAuth1.0
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_URL_NAMESPACE = 'social'

SOCIAL_AUTH_FACEBOOK_APP = decoupled('OAUTH_FACEBOOK_APP',default='')
SOCIAL_AUTH_FACEBOOK_SECRET = decoupled('OAUTH_FACEBOOK_PKI',default='')
SOCIAL_AUTH_FACEBOOK_SCOPE = decoupled('OAUTH_FACEBOOK_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': ','.join([
        # public_profile
        'id', 'cover', 'name', 'first_name', 'last_name', 'age_range', 'link',
        'gender', 'locale', 'picture', 'timezone', 'updated_time', 'verified',
        # extra fields
        'email',
    ]),
}

SOCIAL_AUTH_GOOGLE_OAUTH2_APP = decoupled('OAUTH_GOOGLE_APP',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = decoupled('OAUTH_GOOGLE_PKI',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_PROJECT = decoupled('OAUTH_GOOGLE_KEY',default='')
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = decoupled('OAUTH_GOOGLE_ACL',cast=lambda v: [s.strip() for s in v.split(',')],default='')

SOCIAL_AUTH_TWITTER_APP = decoupled('OAUTH_TWITTER_APP',default='')
SOCIAL_AUTH_TWITTER_SECRET = decoupled('OAUTH_TWITTER_KEY',default='')

SUIT_CONFIG = {
    # header
    'ADMIN_NAME': APP_NAME,
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    # forms
    'SHOW_REQUIRED_ASTERISK': True,  # Default True
    'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    'SEARCH_URL': '/admin/auth/user/',
    'MENU_ICONS': {
       'sites': 'icon-leaf',
       'auth': 'icon-lock',
    },
    'MENU_OPEN_FIRST_CHILD': False, # Default True
    #'MENU_EXCLUDE': (
    #    'auth.group',
    #),
    'MENU': (
        {'label': 'Access', 'icon':'icon-lock', 'models': (
            'authtoken.token','speak.bottoken',
            'social_django.association','social_django.nonce','social_django.usersocialauth',
        )},
        {'label': 'Formal', 'icon':'icon-user', 'models': (
            'basic.identity','speak.botagent',
            'auth.group','basic.organism',
        )},
        {'label': 'Entity', 'icon':'icon-briefcase', 'models': (
            'speak.botlexer','speak.botvocab',
        )},
        {'label': 'Linked', 'icon':'icon-globe', 'models': (
            'daten.backend','daten.endpoint',
        )},
        {'label': 'Device', 'icon':'icon-hdd', 'models': (
            'opsys.machine','opsys.phone','opsys.router',
        )},
        {'label': 'SysAdm', 'icon':'icon-signal', 'models': (
            'basic.backend','opsys.website',
            'scheduler.repeatablejob','scheduler.scheduledjob',
        )},
        {'label': 'Runner', 'app': 'wooey', 'icon':'icon-tasks'},
        {'label': 'Queues', 'icon':'icon-cog', 'url': '/clock/'},
    ),
    #'MENU': (
    #    'sites',
    #    {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #    {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #    {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    #),
    'LIST_PER_PAGE': 50
}

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.history.HistoryPanel',
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_yaml.parsers.YAMLParser',
        'rest_framework_xml.parsers.XMLParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',

        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework_csv.renderers.CSVRenderer',
        'rest_framework_msgpack.renderers.MessagePackRenderer',
        'rest_framework_xml.renderers.XMLRenderer',
        'rest_framework_yaml.renderers.YAMLRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework_datatables.filters.DatatablesFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_datatables.pagination.DatatablesPageNumberPagination',

    'DEFAULT_AUTHENTICATION_CLASSES': [
       'django2use.nerve.Signature.MyAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        #'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
    ],
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'PAGE_SIZE': 50,
}

JWT_AUTH = {
    'JWT_ENCODE_HANDLER':              'rest_framework_jwt.utils.jwt_encode_handler',
    'JWT_DECODE_HANDLER':              'rest_framework_jwt.utils.jwt_decode_handler',
    'JWT_PAYLOAD_HANDLER':             'rest_framework_jwt.utils.jwt_payload_handler',
    'JWT_PAYLOAD_GET_USER_ID_HANDLER': 'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',
    'JWT_RESPONSE_PAYLOAD_HANDLER':    'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': timedelta(seconds=300),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,

}

OAUTH2_PROVIDER = {
    'OIDC_ENABLED': True,

    # this is the list of available scopes
    'SCOPES': {
        'read': 'Read scope',
        'write': 'Write scope',
        'groups': 'Access to your groups',
    },
    'DEFAULT_SCOPES': ['read', 'write'],
}

CORS_ORIGIN_ALLOW_ALL = True

# define basic SEO settings, see
DJANGO_CHECK_SEO_SETTINGS = {
    "content_words_number": [300, 600],
    "internal_links": 1,
    "external_links": 1,
    "meta_title_length": [30, 60],
    "meta_description_length": [50, 160],
    "keywords_in_first_words": 50,
    "max_link_depth": 4,
    "max_url_length": 70,
}

# define auth data (for .htaccess files)
DJANGO_CHECK_SEO_AUTH = {}

# see https://github.com/kapt-labs/django-check-seo/issues/43 for more informations
DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS = False

# update redirect with authentication strategy with value from projectname/settings.py
DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS = DJANGO_CHECK_SEO_AUTH_FOLLOW_REDIRECTS

# define http(s) settings (default = use https)
DJANGO_CHECK_SEO_FORCE_HTTP = False

# update http(s) settings with value from projectname/settings.py
DJANGO_CHECK_SEO_FORCE_HTTP = False

# define css selector to search content into (used for retrieving main content of the page)
DJANGO_CHECK_SEO_SEARCH_IN = {
    "type": "exclude",
    "selectors": ["header", ".cover-section", "#footer"],
}

#
DJANGO_CHECK_SEO_EXCLUDE_CONTENT = ""

CORS_ALLOWED_ORIGINS = ["https://"+APP_FQDN] + [
    "https://%s.%s" % (x,APP_FQDN) for x in (
        'oauth2','manager',
        'apis',
        'cpanel',
    )
]

if False:
    CORS_ALLOWED_ORIGIN_REGEXES = [
        r"^https://([\w\d]+)\.%s$" % APP_FQDN.replace('.','\.'),
    ]

CORS_ALLOW_METHODS = [
    "DELETE",
    "GET",
    "OPTIONS",
    "PATCH",
    "POST",
    "PUT",
]

from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = list(default_headers) + [
    "my-custom-header",
]

DYNAMICDNS_PROVIDERS = {
    'dummy': {
        'plugin': 'django2use.xtend.DNS.Dummy',
    },
    'rackspace': {
        'plugin': 'django2use.xtend.DNS.Rackspace',
        'username': 'YOUR_USERNAME',
        'api_key': 'YOUR_API_KEY',
    },
    'digitalocean': {
        'plugin': 'django2use.xtend.DNS.DigitalOcean',
        'client_id': 'YOUR_CLIENT_ID',
        'api_key': 'YOUR_API_KEY',
    },
}

DEFAULT_HOST = 'cpanel' # .%s' % APP_FQDN
